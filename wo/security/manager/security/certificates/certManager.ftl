# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

certmgr-tab-servers =
    .label = Serwëër yi
certmgr-detail-general-tab-title =
    .label = Ëmb lépp
    .accesskey = p
certmgr-detail-pretty-print-tab-title =
    .label = Detaay yi
    .accesskey = D
certmgr-cert-detail-o =
    .value = Mbootaay
certmgr-cert-detail-org = Mbootaay
certmgr-delete-cert =
    .title = Màbb benn sarsifikaa
    .style = width: 48em; height: 24em;
certmgr-cert-server =
    .label = Serwëër
certmgr-token-name =
    .label = Wër ndombo kaarange
certmgr-begins-value =
    .value = { certmgr-begins-label.label }
certmgr-expires-value =
    .value = { certmgr-expires-label.label }
certmgr-email =
    .label = Màkkaanu imeel
certmgr-view =
    .label = Wone…
    .accesskey = W
certmgr-export =
    .label = jàllale…
    .accesskey = j
certmgr-delete =
    .label = Màbb...
    .accesskey = M
certmgr-backup =
    .label = Aar…
    .accesskey = A
certmgr-restore =
    .label = Fat…
    .accesskey = F
certmgr-add-exception =
    .label = Yokk lu kenn tëgaalewul…
    .accesskey = Y
exception-mgr-cert-location-url =
    .value = Màkkaan :

## PKCS#12 file dialogs


## Import certificate(s) file dialog

import-ca-certs-prompt = Fal ab dencukaay bu am benn walla ñaari sarsifikaa AC bu (yu) wara fatu
import-email-cert-prompt = Fal ab dencukaay bu am sarsifikaa imeel bu wara fatu

## For editing certificates trust


## For Deleting Certificates

delete-user-cert-title =
    .title = Far say sarsifikaa
delete-user-cert-confirm = Ndax bëgg dëgg far sasifikaa yii?
delete-ssl-cert-confirm = Ndax bëgg nga dëgg màbb serwëër yi kenn tëggaalewul woon?
delete-email-cert-title =
    .title = Màbb sarsifikaa imeel
delete-email-cert-confirm = Ndax bëgg nga dëgg màbb sarsifikaa imeel yu nit ñii?

## Cert Viewer

# Add usage
verify-ssl-client =
    .value = Sarsifika kiliyaŋ SSL
verify-ssl-server =
    .value = Sarsifika serwëër SSL
verify-ssl-ca =
    .value = Kelifa dëggal SSL
verify-email-signer =
    .value = Sarsifikaa xaatim bataaxal
verify-email-recip =
    .value = Sarsifikaa jot bataaxal

## Add Security Exception dialog

add-exception-valid-short = Sarsifika bu dëggu
add-exception-checking-short = Settantal xibaar yi

## Certificate export "Save as" and error dialogs

