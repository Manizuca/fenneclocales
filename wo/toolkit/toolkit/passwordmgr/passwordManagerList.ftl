# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

window-close =
    .key = w
focus-search-shortcut =
    .key = f
focus-search-altshortcut =
    .key = k
copy-password-cmd =
    .label = Duppi baatu jàll bi
    .accesskey = D
column-heading-site =
    .label = Dal
column-heading-username =
    .label = Turu jëfandikookat
column-heading-password =
    .label = Baatu jàll
remove =
    .label = Màbbal
    .accesskey = M
import =
    .label = Fat…
    .accesskey = F
close-button =
    .label = Tëj
    .accesskey = T
show-passwords =
    .label = Wone Baatu-jàll yi
    .accesskey = P
hide-passwords =
    .label = Nëbb Baatu-jàll yi
    .accesskey = N
logins-description-all = Duggukaayi dal yiy toftal ci sa nosukaay lañ leen denc
logins-description-filtered = Duggukaay yiy toftal ñoo méngóo ak sag ceet:
remove-all =
    .label = Far Lépp
    .accesskey = A
remove-all-shown =
    .label = Far Wonéef Gépp
    .accesskey = F
remove-all-passwords-prompt = Wóor na ne dangaa bëgg a far baati-jàll yépp?
remove-all-passwords-title = Far baati-jàll yépp
no-master-password-prompt = Wóor na ne dangaa bëgg a wone say baati-jàll?
