# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

addons-window =
    .title = Saytukatu modili mottali yi
search-header-shortcut =
    .key = f
loading-label =
    .value = Yeb mi…
list-empty-installed =
    .value = Amuloo benn modil bu mel nii te sampu
list-empty-available-updates =
    .value = Amul benn yeesal
list-empty-recent-updates =
    .value = Toog nga lu yàgg yeesaloo say modili mottali
list-empty-find-updates =
    .label = Wut ay yeesal
list-empty-button =
    .label = Gënati jàngaat ci modili mottali yi
install-addon-from-file =
    .label = Samp modil dale ko ci…
    .accesskey = S
tools-menu =
    .tooltiptext = Jumtukaayi modil yépp
cmd-show-details =
    .label = Wone xibaar yi
    .accesskey = W
cmd-find-updates =
    .label = Wut yeesal yi
    .accesskey = y
cmd-preferences =
    .label =
        { PLATFORM() ->
            [windows] Tannéef yi
           *[other] Taamu yi
        }
    .accesskey =
        { PLATFORM() ->
            [windows] T
           *[other] T
        }
cmd-enable-theme =
    .label = Def tem bi
    .accesskey = D
cmd-disable-theme =
    .label = Dindi tem bi
    .accesskey = D
cmd-install-addon =
    .label = Samp
    .accesskey = S
cmd-contribute =
    .label = Wàll si
    .accesskey = W
    .tooltiptext = Wàll si ci yokkute modilu mottali bi
discover-title = Yan ñooy modili mottali yi
discover-description =
    Modili mottali yi ay jëfekaay lañu yu lay may nga solal { -brand-short-name }
    ak gëddë walla melin yi yokku. Gañeel diir ak benn pano wet,seet metewo walla nga soppi melokaanu { -brand-short-name }
    numu la neexe.
discover-footer =
    Bu sa jokkoo internet suqallikoo, pano bi dina la may nga gis
    yenn ci modili mottali yi gën te gëna siiw.
detail-version =
    .label = Sumb
detail-last-updated =
    .label = Yeesal bi mujj
detail-contributions-description = Tàllalkatu modil  day ñaan nga dimbali ko as tuut ngir mu mottali liggéey bi
detail-update-type =
    .value = Yeesali boppam
detail-update-default =
    .label = Ñakk ndigal
    .tooltiptext = Yeesal yi dina ñu sampul seen bopp su fekkeeni regalaasu ñakk ndigal la
detail-update-automatic =
    .label = Suqali
    .tooltiptext = Sampal boppam yeesal yi
detail-update-manual =
    .label = Suux
    .tooltiptext = Du sampal boppam yeesal yi
detail-home =
    .label = Xëtu dalal
detail-home-value =
    .value = { detail-home.label }
detail-repository =
    .label = Jëmmalinu modil bi
detail-repository-value =
    .value = { detail-repository.label }
detail-check-for-updates =
    .label = Wut ay yeesal yu féex
    .accesskey = W
    .tooltiptext = Wut ay yeesal yu féex pur modil bi
detail-show-preferences =
    .label =
        { PLATFORM() ->
            [windows] Tannéef yi
           *[other] Taamu yi
        }
    .accesskey =
        { PLATFORM() ->
            [windows] T
           *[other] T
        }
    .tooltiptext =
        { PLATFORM() ->
            [windows] Soppali tànnéefi modil yi
           *[other] Soppi taamu modil bi
        }
detail-rating =
    .value = Yëgg
addon-restart-now =
    .label = Tambaliwaatal léegi
extensions-view-discover =
    .name = Katalog
    .tooltiptext = { extensions-view-discover.name }
extensions-view-recent-updates =
    .name = Yeesal
    .tooltiptext = { extensions-view-recent-updates.name }
extensions-view-available-updates =
    .name = Yeesal yi féex
    .tooltiptext = { extensions-view-available-updates.name }

## These are global warnings

extensions-warning-safe-mode-label =
    .value = Anamu ñakk lajj suuxal na modili yokk yépp.
extensions-warning-safe-mode-container =
    .tooltiptext = { extensions-warning-safe-mode-label.value }
extensions-warning-check-compatibility-label =
    .value = Wóorliku andu modili mottali yi dafa suux. Xëyna dafa am ay modil yu mënula and.
extensions-warning-check-compatibility-container =
    .tooltiptext = { extensions-warning-check-compatibility-label.value }
extensions-warning-check-compatibility-enable =
    .label = Suqali
    .tooltiptext = Suqali seetlu yi ànd
extensions-warning-update-security-label =
    .value = Wóoralu yeesali kaarange modili mottali yi dafa suux. Xëyna da nga am ay modili mottali yu wóorul.
extensions-warning-update-security-container =
    .tooltiptext = { extensions-warning-update-security-label.value }
extensions-warning-update-security-enable =
    .label = Suqali
    .tooltiptext = Suqali wóoralu yeesali kaaraange yu modili mottali yi

## Strings connected to add-on updates

extensions-updates-check-for-updates =
    .label = Wut ay yeesal
    .accesskey = W
extensions-updates-view-updates =
    .label = Wone yeesal yi bees
    .accesskey = W

# This menu item is a checkbox that toggles the default global behavior for
# add-on update checking.

extensions-updates-update-addons-automatically =
    .label = Yeesal boppam modil yi
    .accesskey = Y

## Specific add-ons can have custom update checking behaviors ("Manually",
## "Automatically", "Use default global behavior"). These menu items reset the
## update checking behavior for all add-ons to the default global behavior
## (which itself is either "Automatically" or "Manually", controlled by the
## extensions-updates-update-addons-automatically.label menu item).

extensions-updates-reset-updates-to-automatic =
    .label = Suqali yeesalu boppam bu bépp modil
    .accesskey = S
extensions-updates-reset-updates-to-manual =
    .label = Suuxal yeesalu boppam bu bépp modil
    .accesskey = S

## Status messages displayed when updating add-ons

extensions-updates-updating =
    .value = Yeesal modil yi
extensions-updates-installed =
    .value = Say modil yees nañu.
extensions-updates-downloaded =
    .value = Say yeesali modil yebu nañu.
extensions-updates-restart =
    .label = Tambaliwaatal ngir sottal samp wi.
extensions-updates-none-found =
    .value = Amul benn yeesal bu féex
extensions-updates-manual-updates-found =
    .label = Wone yeesal yi féex
extensions-updates-update-selected =
    .label = Samp yeesal yi
    .tooltiptext = Samp yeesal yi féex ci lim bii
