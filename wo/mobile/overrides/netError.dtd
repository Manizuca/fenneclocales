<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY % brandDTD SYSTEM "chrome://branding/locale/brand.dtd">
%brandDTD;

<!ENTITY loadError.label "Njuumte ci yebug xët wi">
<!ENTITY retry.label "Jéemaatal">

<!-- Specific error messages -->

<!ENTITY connectionFailure.title "Jokkoo gi antuwul">
<!ENTITY connectionFailure.longDesc2 "&sharedLongDesc3;">

<!ENTITY deniedPortAccess.title "Màkkaan bi dañu ko tere">
<!ENTITY deniedPortAccess.longDesc "">

<!ENTITY dnsNotFound.title "Joxekaay bi jàppandiwul">
<!-- LOCALIZATION NOTE (dnsNotFound.longDesc4) This string contains markup including widgets for searching
     or enabling wifi connections. The text inside tags should be localized.  Do not change the ids. -->
<!ENTITY dnsNotFound.longDesc4 "
<ul>
  <li>Saytuwaatal màkkaan gi ngir njuumteeg mbindiin gu mel ni 
    <strong>ww</strong>.example.com te waroon doon
    <strong>www</strong>.example.com</li>
    <div id='searchbox'>
      <input id='searchtext' type='search'></input>
      <button id='searchbutton'>Seet</button>
    </div>
  <li>Soo manul fexe ba joow ci benn dal, xoolal internet bu sa jumtukaay walla sa lonkoo Wi-Fi .
    <button id='wifi'>Taal Wi-Fi bi</button>
  </li>
</ul>
">

<!ENTITY fileNotFound.title "Dencukaay bi nekkatufi">
<!ENTITY fileNotFound.longDesc "<ul> 
<li>Seetaatal mbindu turu dencukaay bi.</li> 
<li>Seetal ndax kenn randalul dencukaay bi, walla ñu jox ko beneen tur mbaa ñu far ko.</li> 
</ul>">

<!ENTITY fileAccessDenied.title "Bañees nañ jotug dencukaay bi">
<!ENTITY fileAccessDenied.longDesc "<ul> 
  <li>Dañ koy war a far, randal walla sañaluñ ku jot ci dencukaay bi.</li>
 </ul>
  
  ">

<!ENTITY generic.title "Oops.">
<!ENTITY generic.longDesc "<p>Xamuñu lu tax, &brandShortName; manul yeb xët wi.</p>">

<!ENTITY malformedURI.title "Makkan gi jagul">
<!-- LOCALIZATION NOTE (malformedURI.longDesc2) This string contains markup including widgets for searching
     or enabling wifi connections. The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY malformedURI.longDesc2 "
<ul>
  <li>Màkkaani web yi naka-jekk nii lañ leen di bindee
    <strong>http://www.example.com/</strong></li>
    <div id='searchbox'>
      <input id='searchtext' type='search'></input>
      <button id='searchbutton'>Seet</button>
    </div>
  <li>Wóorluwul ci ne galan bi wengee kanam nga jëfandikoo(i.e.
    <strong>/</strong>).</li>
</ul>
">

<!ENTITY netInterrupt.title "Lonkoo gi dafa dog">
<!ENTITY netInterrupt.longDesc2 "&sharedLongDesc3;">

<!ENTITY notCached.title "Jukki bi jeexal na">
<!ENTITY notCached.longDesc "<p>Jukki bi ñu laaj amatu ci &brandShortName;'.</p><ul><li>ngir kaarange, &brandShortName; du tëb rekk laj yenn jukki yi.</li><li>Cuqal ci jeemat ngir laajaat jukki bi ci dal bi.</li></ul>">

<!ENTITY netOffline.title "Anamu jokkoodiku">
<!-- LOCALIZATION NOTE (netOffline.longDesc3) This string contains markup including widgets enabling wifi connections.
     The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY netOffline.longDesc3 "
<ul>
  <li>Try again. &brandShortName; will attempt to open a connection and reload the page.
    <button id='wifi'>Enable Wi-Fi</button>
  </li>
</ul>
">

<!ENTITY contentEncodingError.title "Njumte ci suulaale këmb gi">
<!ENTITY contentEncodingError.longDesc "<ul> 
<li>Jéemal wax ak boroom dalub web bi ngir biral ko jafe jafe bi.</li> 
</ul>">

<!ENTITY unsafeContentType.title "Giiri dencukaay yii wóoru ñu">
<!ENTITY unsafeContentType.longDesc "<ul> 
<li>Jéemal wax ak boroom dalub web bi ngir biral ko jafe jafe bi.</li> 
</ul>">

<!ENTITY netReset.title "Yeesalaat nanu jokkoo bi">
<!ENTITY netReset.longDesc2 "&sharedLongDesc3;">

<!ENTITY netTimeout.title "Diiru jokkoo bi jeex na">
<!ENTITY netTimeout.longDesc2 "&sharedLongDesc3;">

<!ENTITY unknownProtocolFound.title "The address wasn't understood">
<!ENTITY unknownProtocolFound.longDesc "
<ul>
  <li>You might need to install other software to open this address.</li>
</ul>
">

<!ENTITY proxyConnectFailure.title "Servëër proxy bi nanguwul jokkoo bi">
<!ENTITY proxyConnectFailure.longDesc "<ul> 
<li>Seetal ndax paraameetaru proxy bi jaar nanu yoon.</li> 
<li>Jokkool ak sa saytukatu lëkkale ngir xam ndax serwëër proxy baa ngiy dox .</li> 
</ul>">

<!ENTITY proxyResolveFailure.title "Kenn mënula gis serwëër proxy bi">
<!-- LOCALIZATION NOTE (proxyResolveFailure.longDesc3) This string contains markup including widgets for enabling wifi connections.
     The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY proxyResolveFailure.longDesc3 "
<ul>
  <li>Check the proxy settings to make sure that they are correct.</li>
  <li>Check to make sure your device has a working data or Wi-Fi connection.
    <button id='wifi'>Enable Wi-Fi</button>
  </li>
</ul>
">

<!ENTITY redirectLoop.title "Xët wi kenn jubaluko bu baax">
<!ENTITY redirectLoop.longDesc "<ul> 
<li>Li indi jafe jafe bii mën na nekk suuxal mba bañ 
kuki.</li> 
</ul>">

<!ENTITY unknownSocketType.title "Tontu bu kenn xalaatulwoon ci serwëër bi">
<!ENTITY unknownSocketType.longDesc "<ul> 
<li>Seetal ndax sa saytukatu kaarange bopp
sampu na ci sa doxalin.</li> 
<li>Mën na doon tabbinu serwëër bi la.</li> 
</ul>">

<!ENTITY nssFailure2.title "Jokkoo bu and ak kaarange lajj na">
<!ENTITY nssFailure2.longDesc2 "
<ul>
  <li>The page you are trying to view cannot be shown because the authenticity of the received data could not be verified.</li>
  <li>Please contact the website owners to inform them of this problem.</li>
</ul>
">

<!ENTITY nssBadCert.title "Jokkoo bu and ak kaarange lajj na">
<!ENTITY nssBadCert.longDesc2 "<ul> 
<li>Lii më na nekk ni ñu tabbe serwëër bi walla mu nekk nit kuy jeema nangu dantite serwëër bi 
.</li> 
<li>Su fekkee mës nga jokko ak serwëër bile, mën na am njumte bi du yagg
mën nga jéemaat ci kanam.</li> 
</ul>">

<!-- LOCALIZATION NOTE (sharedLongDesc3) This string contains markup including widgets for enabling wifi connections.
     The text inside the tags should be localized. Do not touch the ids. -->
<!ENTITY sharedLongDesc3 "
<ul>
  <li>The site could be temporarily unavailable or too busy. Try again in a few moments.</li>
  <li>If you are unable to load any pages, check your mobile device's data or Wi-Fi connection.
    <button id='wifi'>Enable Wi-Fi</button>
  </li>
</ul>
">

<!ENTITY cspBlocked.title "Blocked by Content Security Policy">
<!ENTITY cspBlocked.longDesc "<p>&brandShortName; prevented this page from loading in this way because the page has a content security policy that disallows it.</p>">

<!ENTITY corruptedContentErrorv2.title "Njuumteg Ëmbéef gees Ger">
<!ENTITY corruptedContentErrorv2.longDesc "<p>Xët wi ngay jéem a xool maneesu koo wone ndax ag njuumte gees nemmeeku ci yónneeg njoxéef yi.</p><ul><li>Di la sàkku nga jéem a jot boroom dal bi ngir yegge ko gii jafe-jafe.</li></ul>">

<!ENTITY securityOverride.linkText "Walla mën nga yokk lu kenn tëggaalewul…">
<!ENTITY securityOverride.getMeOutOfHereButton "Génn xët wi!">
<!ENTITY securityOverride.exceptionButtonLabel "Yokk lu kenn tëggaalewul…">

<!-- LOCALIZATION NOTE (securityOverride.warningContent) - Do not translate the
contents of the <xul:button> tags.  The only language content is the label= field,
which uses strings already defined above. The button is included here (instead of
netError.xhtml) because it exposes functionality specific to firefox. -->

<!ENTITY securityOverride.warningContent "<p>Waruloo yokk lu kenn tëggaalewul boo dee jëfandikoo jokkoo internet boo wóoluwul walla boo tammul di jot ay artu ci serwëër bi .</p> 
 
<button id='getMeOutOfHereButton'>&securityOverride.getMeOutOfHereButton;</button> 
<button id='exceptionDialogButton'>&securityOverride.exceptionButtonLabel;</button>">

<!ENTITY remoteXUL.title "XUL bu sori">
<!ENTITY remoteXUL.longDesc "<p><ul><li>Jokkool ak boroomi dalub web ngir yëgal leen jafe jafe bi.</li></ul></p>">

<!ENTITY sslv3Used.title "Lonku cig karaange antuwul">
<!-- LOCALIZATION NOTE (sslv3Used.longDesc) - Do not translate
     "SSL_ERROR_UNSUPPORTED_VERSION". -->
<!ENTITY sslv3Used.longDesc "Yeneeni xibaar: SSL_ERROR_UNSUPPORTED_VERSION">

<!ENTITY weakCryptoUsed.title "Sa lonkoo amul kaaraange">
<!-- LOCALIZATION NOTE (weakCryptoUsed.longDesc) - Do not translate
     "SSL_ERROR_NO_CYPHER_OVERLAP". -->
<!ENTITY weakCryptoUsed.longDesc "Yeneeni xibaar: SSL_ERROR_NO_CYPHER_OVERLAP">

<!ENTITY inadequateSecurityError.title "Sa lonkoo amul kaaraange">
<!-- LOCALIZATION NOTE (inadequateSecurityError.longDesc) - Do not translate
     "NS_ERROR_NET_INADEQUATE_SECURITY". -->
<!ENTITY inadequateSecurityError.longDesc "<p><span class='hostname'></span> dafay jëfandikoo ay xarala yu yagg walla yu yomb a song. Ab songkat di na man a siiwal ci lu yomb ay xibaar yoo foogoon ne aar nañ leen. Yorkatu dalu web bi faww mu jubbanti joxekaay bi laataa nga koy man a yër.</p><p> Njuumte: NS_ERROR_NET_INADEQUATE_SECURITY</p>">
