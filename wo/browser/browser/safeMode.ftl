# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

safe-mode-window =
    .title = Anam wu amul lajj bu { -brand-short-name }
    .style = max-width: 400px
start-safe-mode =
    .label = Tambalil ci anam budul lajj
safe-mode-description = Anam budul lajj anam bu beru la bu { -brand-short-name } mën nañu ko jariñoo ngir saafara yenn yi.
