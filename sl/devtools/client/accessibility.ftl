# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Več o tem

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Opozorilo
accessibility-fail =
    .alt = Napaka

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-document-title = Dokumenti morajo imeti <code>naslov</code>. <a>Več o tem</a>
accessibility-text-label-issue-heading = Naslovi morajo biti označeni. <a>Več o tem</a>
