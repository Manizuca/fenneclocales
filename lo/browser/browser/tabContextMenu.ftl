# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

reload-tab =
    .label = ໂຫລດແທັບໃຫມ່
    .accesskey = R
duplicate-tab =
    .label = ເປີດແທັບທີ່ຊ້ຳກັນ
    .accesskey = D
close-tabs-to-the-end =
    .label = ປິດແທັບໄປທາງຂວາ
    .accesskey = i
close-other-tabs =
    .label = ປິດແທັບອື່ນໆ
    .accesskey = o
pin-tab =
    .label = ປັກຫມຸດແທັບ
    .accesskey = P
unpin-tab =
    .label = ຖອນຫມຸດແທັບ
    .accesskey = b
pin-selected-tabs =
    .label = ປັກຫມຸດແທັບ
    .accesskey = P
unpin-selected-tabs =
    .label = ຖອນຫມຸດແທັບ
    .accesskey = b
bookmark-selected-tabs =
    .label = ບຸກມາກແທັບ...
    .accesskey = k
reopen-in-container =
    .label = ເປີດຄືນໃຫມ່ໃນການແຍກຂໍ້ມູນ
    .accesskey = e
move-to-new-window =
    .label = ຍ້າຍໄປວິນໂດໃໝ່
    .accesskey = ວ
undo-close-tab =
    .label = ເອີ້ນຄືນແທັບທີ່ຫາກໍປິດ
    .accesskey = U
close-tab =
    .label = ປິດແທັບ
    .accesskey = C
