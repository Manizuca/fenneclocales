# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fonts-window =
    .title = ແບບຕົວຫນັງສື
fonts-window-close =
    .key = w

## Font groups by language

fonts-langgroup-header = ແບບຕົວຫນັງສືສຳລັບ
    .accesskey = F
fonts-langgroup-arabic =
    .label = ພາສາອາຣັບ
fonts-langgroup-armenian =
    .label = ພາສາອາກເມນີ
fonts-langgroup-bengali =
    .label = ພາສາບັງກະລາເດດ
fonts-langgroup-simpl-chinese =
    .label = ພາສາຈີນຕົວຫຍໍ້
fonts-langgroup-trad-chinese-hk =
    .label = ພາສາຈີນຕົວເຕັມ (ຮົງກົງ)
fonts-langgroup-trad-chinese =
    .label = ພາສາຈີນຕົວເຕັມ (ໄຕ້ຫວັນ)
fonts-langgroup-cyrillic =
    .label = ຊີຣິລລິກ
fonts-langgroup-devanagari =
    .label = ເທວະນາຄຣີ
fonts-langgroup-ethiopic =
    .label = ເອທິໂອເປຍ
fonts-langgroup-georgian =
    .label = ຈໍເຈຍ
fonts-langgroup-el =
    .label = ເກ​ຣັກ
fonts-langgroup-gujarati =
    .label = ກູຈາຣາຕີ
fonts-langgroup-gurmukhi =
    .label = ກູມູຄີ
fonts-langgroup-japanese =
    .label = ຍີ່ປຸ່ນ
fonts-langgroup-hebrew =
    .label = ຢິວ
fonts-langgroup-kannada =
    .label = ກັນນາດາ
fonts-langgroup-khmer =
    .label = ຂະແມ
fonts-langgroup-korean =
    .label = ເກົາຫຼີ
# Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language.
fonts-langgroup-latin =
    .label = ລາຕິນ
fonts-langgroup-malayalam =
    .label = ​ມາ​ລາ​ຢາ​ລຳ
fonts-langgroup-math =
    .label = ຄະນິດສາດ
fonts-langgroup-odia =
    .label = ໂອເດຍ
fonts-langgroup-sinhala =
    .label = ຊິນ​ຮາ​ລາ
fonts-langgroup-tamil =
    .label = ທະມິນ
fonts-langgroup-telugu =
    .label = ເຕລູກູ
fonts-langgroup-thai =
    .label = ໄທ
fonts-langgroup-tibetan =
    .label = ຕິເບດ
fonts-langgroup-canadian =
    .label = ອັກສອນພະຍາງພື້ນເມືອງການາດາ
fonts-langgroup-other =
    .label = ລະບົບການຂຽນອື່ນໆ

## Default fonts and their sizes

fonts-proportional-header = ຄວາມກວ້າງຕາມສັດສ່ວນ
    .accesskey = P
fonts-default-serif =
    .label = Serif
fonts-default-sans-serif =
    .label = Sans-serif
fonts-proportional-size = ຂະຫນາດ
    .accesskey = ຂ
fonts-serif = Serif
    .accesskey = S
fonts-sans-serif = Sans-serif
    .accesskey = n
fonts-monospace = ຄວາມກວ້າງຄົງທີ່
    .accesskey = M
fonts-monospace-size = ຂະຫນາດ
    .accesskey = ຂ
fonts-minsize = ຂະຫນາດແບບຕົວຫນັງສືຕ່ຳສຸດ
    .accesskey = o
fonts-minsize-none =
    .label = ບໍ່ມີ
fonts-allow-own =
    .label = ອະນຸຍາດໃຫ້ຫນ້າຕ່າງໆເລືອກໃຊ້ແບບຕົວຫນັງສືຂອງຕົວເອງ, ແທນທີ່ຈະເປັນທີ່ທ່ານເລືອກໄວ້ດ້ານເທິງ
    .accesskey = A

## Text Encodings
##
## Translate the encoding names as adjectives for an encoding, not as the name
## of the language.

fonts-languages-fallback-header = ການເຂົ້າລະຫັດຂໍ້ຄວາມສຳລັບເນື້ອຫາແບບເກົ່າ
fonts-languages-fallback-desc = ການເຂົ້າລະຫັດຂໍ້ຄວາມນີ້ຖືກໃຊ້ສຳລັບເນື້ອຫາແບບເກົ່າທີ່ບໍ່ສາມາດປະກາດການເຂົ້າລະຫັດໄດ້.
fonts-languages-fallback-label = ການເຂົ້າລະຫັດຂໍ້ຄວາມທີ່ໃຊ້ສະແດງແທນ
    .accesskey = T
fonts-languages-fallback-name-auto =
    .label = ຄ່າເລີ່ມຕົ້ນສຳລັບພາສາຖິ່ນປະຈຸບັນ
fonts-languages-fallback-name-arabic =
    .label = ອາຣັບ
fonts-languages-fallback-name-baltic =
    .label = ບາລຕິກ
fonts-languages-fallback-name-ceiso =
    .label = ເອີລົບກາງ, ISO
fonts-languages-fallback-name-cewindows =
    .label = ເອີລົບກາງ, Microsoft
fonts-languages-fallback-name-simplified =
    .label = ຈີນຕົວຫຍໍ້
fonts-languages-fallback-name-traditional =
    .label = ຈີນຕົວເຕັມ
fonts-languages-fallback-name-cyrillic =
    .label = ຊີຣິລລິກ
fonts-languages-fallback-name-greek =
    .label = ເກ​ຣັກ
fonts-languages-fallback-name-hebrew =
    .label = ຢິວ
fonts-languages-fallback-name-japanese =
    .label = ຍີ່ປຸ່ນ
fonts-languages-fallback-name-korean =
    .label = ເກົາຫຼີ
fonts-languages-fallback-name-thai =
    .label = ໄທ
fonts-languages-fallback-name-turkish =
    .label = ຕວກກີ
fonts-languages-fallback-name-vietnamese =
    .label = ຫວຽດນາມ
fonts-languages-fallback-name-other =
    .label = ອື່ນໆ (ລວມທັງເອີລົບຕາເວັນຕົກ)
fonts-very-large-warning-title = ຂະຫນາດແບບຕົວຫນັງສືຕ່ຳສຸດທີ່ໃຫຍ່
fonts-very-large-warning-message = ທ່ານໄດ້ເລືອກຂະຫນາດຕ່ຳສຸດທີ່ຂະຫນາດນ້ອຍທີ່ສຸດ (ຫຼາຍກວ່າ 24 pixels). ນີ້ອາດເຮັດໃຫ້ມັນຍາ ກຫລື ເປັນໄປບໍ່ໄດ້ທີ່ຈະນໍາໃຊ້ຫນ້າຕັ້ງຄ່າທີ່ສໍາຄັນຕ່າງໆເຊັ່ນນີ້.
fonts-very-large-warning-accept = ເກັບການປ່ຽນແປງຂອງຂ້ອຍຕໍ່ໄປ
# Variables:
#   $name {string, "Arial"} - Name of the default font
fonts-label-default =
    .label = ຄ່າເລີ່ມຕົ້ນ ({ $name })
fonts-label-default-unnamed =
    .label = ຄ່າເລີ່ມຕົ້ນ
