# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

blocklist-window =
    .title = ລາຍການບັອກ
    .style = width: 55em
blocklist-desc = ທ່ານສາມາດເລືອກລາຍການໃດທີ່ { -brand-short-name } ຈະໃຊ້ເພື່ອບັອກອົງປະກອບຂອງເວັບທີ່ອາດຈະຕິດຕາມກິດຈະກຳການທ່ອງເວັບຂອງທ່ານ.
blocklist-close-key =
    .key = w
blocklist-treehead-list =
    .label = ລາຍການ
blocklist-button-cancel =
    .label = ຍົກເລີກ
    .accesskey = ຍ
blocklist-button-ok =
    .label = ບັນທຶກການປ່ຽນແປງ
    .accesskey = ບ
# This template constructs the name of the block list in the block lists dialog.
# It combines the list name and description.
# e.g. "Standard (Recommended). This list does a pretty good job."
#
# Variables:
#   $listName {string, "Standard (Recommended)."} - List name.
#   $description {string, "This list does a pretty good job."} - Description of the list.
blocklist-item-list-template = { $listName } { $description }
blocklist-item-moz-std-name = ການປົກປ້ອງແບບພື້ນຖານຂອງ Disconnect.me(ແນະນຳ)
blocklist-item-moz-std-desc = ອະນຸຍາດຕົວຕິດຕາມບາງສ່ວນເພື່ອໃຫ້ເວັບໄຊເຮັດວຽກໄດ້ຢ່າງຖືກຕ້ອງ.
blocklist-item-moz-full-name = ການປົກປ້ອງແບບເຂັ້ມງວດຂອງ Disconnect.me
blocklist-item-moz-full-desc = ລະງັບຕົວຕິດຕາມທີ່ຮູ້ຈັກ. ເວັບໄຊບາງແຫ່ງອາດເຮັດວຽກບໍ່ຖືກຕ້ອງ.
