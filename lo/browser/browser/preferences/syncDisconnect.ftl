# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

sync-disconnect-dialog =
    .title = ຍົກເລີກການເຊື່ອມຕໍ່ { -sync-brand-short-name } ບໍ່?
    .style = width: 36em; min-height: 35em;
sync-disconnect-heading = ທ່ານຕ້ອງການລຶບຂໍ້ມູນການທ່ອງເວັບອອກຈາກຄອມພິວເຕີນີ້ບໍ່? ຂໍ້ມູນ { -sync-brand-name } ຈະຍັງຄົງມີຢູ່ໃນບັນຊີຂອງທ່ານ.
sync-disconnect-remove-sync-caption = ລຶບຂໍ້ມູນ { -sync-brand-name }
sync-disconnect-remove-sync-data = ບຸກມາກ, ປະຫວັດການໃຊ້ງານ, ລະຫັດຜ່ານ, ແລະອື່ນໆ.
sync-disconnect-remove-other-caption = ລຶບຂໍ້ມູນສ່ວນຕົວອື່ນໆ
sync-disconnect-remove-other-data = ຄຸກກີ້, ແຄຊ, ຂໍ້ມູນເວັບໄຊອອບລາຍ, ແລະອື່ນໆ.
# Shown while the disconnect is in progress
sync-disconnect-disconnecting = ກຳລັງຍົກເລີກການເຊື່ອມຕໍ່…
sync-disconnect-cancel =
    .label = ຍົກເລີກ
    .accesskey = C

## Disconnect confirm Button
##
## The 2 labels which may be shown on the single "Disconnect" button, depending
## on the state of the checkboxes.

sync-disconnect-confirm-disconnect-delete =
    .label = ຍົກເລີກການເຊື່ອມຕໍ່ & ລຶບ
    .accesskey = D
sync-disconnect-confirm-disconnect =
    .label = ຍົກເລີກການເຊື່ອມຕໍ່ຢ່າງດຽວ
    .accesskey = D
