# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = ມີການອັບເດດ { -brand-shorter-name } ໃຫມ່.
    .buttonlabel = ດາວໂຫລດອັບເດດ
    .buttonaccesskey = D
    .secondarybuttonlabel = ບໍ່ແມ່ນຕອນນີ້
    .secondarybuttonaccesskey = N
appmenu-update-available-message = ອັບເດດ { -brand-shorter-name } ເພື່ອຄວາມໄວແລະຄວາມເປັນສ່ວນຕົວລ່າສຸດ.
appmenu-update-whats-new =
    .value = ເບິ່ງວ່າມີຫຍັງໃໝ່.
appmenu-update-restart =
    .label = ເລີ່ມເຮັດວຽກໃຫມ່ເພື່ອອັບເດດ { -brand-shorter-name }.
    .buttonlabel = ເລີ່ມເຮັດວຽກໃຫມ່ແລະກູ້ຄືນ
    .buttonaccesskey = R
    .secondarybuttonlabel = ບໍ່ແມ່ນຕອນນີ້
    .secondarybuttonaccesskey = N
