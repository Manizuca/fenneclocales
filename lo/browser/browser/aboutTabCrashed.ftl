# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

crashed-title = ຕົວລາຍງານບັນຫາແທັບ
crashed-close-tab-button = ປິດແທັບ
crashed-restore-tab-button = ເອີ້ນຄືນແທັບນີ້
crashed-restore-all-button = ກູ້ຄືນແຖບທີ່ເກີດຂໍ້ຂັດຂ້ອງທັງໝົດ
crashed-header = ອ໊ະ. ແຖບຂອງທ່ານໄດ້ຂັດຂ້ອງ.
crashed-offer-help = ພວກເຮົາຊ່ວຍໄດ້!
crashed-single-offer-help-message = ເລືອກ { crashed-restore-tab-button } ເພື່ອໂຫລດຫນ້າໃຫມ່
crashed-multiple-offer-help-message = ເລືອກ { crashed-restore-tab-button } ຫຼື { crashed-restore-all-button } ເພື່ອໂຫລດຫນ້າໃຫມ່
crashed-request-help = ທ່ານຈະຊ່ວຍພວກເຮົາໄດ້ຫຼືບໍ່?
crashed-request-help-message = ລາຍງານຂໍ້ຂັດຂ້ອງຊ່ວຍພວກເຮົາວິນິດໄສບັນຫາແລະສ້າງ { -brand-short-name } ໃຫ້ດີຂຶ້ນ.
crashed-request-report-title = ລາຍງານແທັບນີ້
crashed-send-report = ສົ່ງລາຍງານຂໍ້ຂັດຂ້ອງອັດຕະໂນມັດເພື່ອໃຫ້ເຮົາສາມາດແກ້ໄຂປັນຫາແບບນີ້ໄດ້.
crashed-comment =
    .placeholder = ປະກອບຄຳເຫັນ (ຄຳເຫັນນີ້ແມ່ນທຸກໆຄົນສາມາດເຫັນໄດ້)
crashed-include-URL = ຮວມ URLs ຂອງໄຊ໌ທີ່ເຈົ້າກຳລັງຢ້ຽມຢາມເມື່ອ { -brand-short-name } ຂັດຂ້ອງ
crashed-email-placeholder = ປ້ອນທີ່ຢູ່ອີເມລຂອງທ່ານທີ່ນີ້
crashed-email-me = ສົ່ງອີເມລຫາຂ້ອຍເມື່ອມີຂໍ້ມູນເພີ່ນທີ່ໃຊ້ງານໄດ້
crashed-report-sent = ສົ່ງລາຍງານຂໍ້ຂັດຂ້ອງແລ້ວ; ຂອບໃຈທີ່ຊ່ວຍເຮັດ { -brand-short-name } ໃຫ້ດີຂຶ້ນ
crashed-request-auto-submit-title = ລາຍງານແຖບເບື່ອງຫຼັງ
crashed-auto-submit-checkbox = ອັບເດດການຕັ້ງຄ່າເພື່ອສົ່ງລາຍງານເມື່ອ { -brand-short-name } ຂັດຂ້ອງໂດຍອັດຕະໂນມັດ
