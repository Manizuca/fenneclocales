# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-learn-more = ຮຽນຮູ້ເພີ່ມເຕີມກ່ຽວກັບ <a data-l10n-name="learn-more">ການທ່ອງເວັບແບບສ່ວນຕົວ</a>.
about-private-browsing-info-visited = ເຂົ້າໄປເບິງຫນ້ານີ້ແລ້ວ
privatebrowsingpage-open-private-window-label = ເປີດວິນໂດສ່ວນຕົວ
    .accesskey = ສ
about-private-browsing-info-notsaved = ເມື່ອທ່ານທ່ອງເວັບໃນວິນໂດສ່ວນຕົວ, { -brand-short-name } <strong>ບໍ່ບັນທຶກ</strong>:
about-private-browsing-info-bookmarks = ບຸກມາກ
about-private-browsing-info-searches = ຄົ້ນຫາ
about-private-browsing-info-downloads = ດາວໂຫລດ
private-browsing-title = ການທ່ອງເວັບແບບສ່ວນຕົວ
about-private-browsing-info-saved = { -brand-short-name } <strong>ຈະບັນທຶກ</strong> ຂອງທ່ານ:
about-private-browsing-info-temporary-files = ໄຟລ໌ຊົ່ວຄາວ
about-private-browsing-info-cookies = ຄຸກກີ
tracking-protection-start-tour = ເບິງວິທີການເຮັດວຽກຂອງມັນ
about-private-browsing-note = ການທ່ອງເວັບແບບສ່ວນຕົວ <strong>ບໍ່ເຮັດໃຫ້ທ່ານເປັນອາໂນນິມັດສ໌</strong> ໃນໂລກອິນເຕີເນັດ. ນາຍຈ້າງ ຫລື ຜູ້ໃຫ້ບໍລິການອິນເຕີເນັດຂອງທ່ານຍັງຄົງສາມາດຮູ້ໄດ້ວ່າໄດ້ເຂົ້າໄປເບິງເວັບໃດແດ່.
about-private-browsing-not-private = ປະຈຸບັນນີ້ທ່ານບໍ່ໄດ້ຢູ່ໃນວິນໂດສ່ວນຕົວ.
content-blocking-title = ການລະງັບເນື້ອຫາ
content-blocking-description = ບາງເວັບໄຊທ໌ໃຊ້ຕົວຕິດຕາມທີ່ສາມາດກວດກາກິດຈະກໍາຂອງທ່ານຜ່ານອິນເຕີເນັດ. ໃນວິນໂດສ່ວນຕົວ, ການປິດກັ້ນເນື້ອຫາອັດຕະໂນມັດຂອງ { -brand-short-name } ຈະປິດກັ້ນຕິດຕາມຈໍານວນຫຼາຍທີ່ສາມາດເກັບກຳຂໍ້ມູນກ່ຽວກັບພຶດຕິກໍາການທ່ອງເວັບຂອງທ່ານໂດຍອັດຕະໂນມັດ.
