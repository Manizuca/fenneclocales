# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### UI strings for the simplified onboarding modal / about:welcome
### Various strings use a non-breaking space to avoid a single dangling /
### widowed word, so test on various window sizes if you also want this.


## These button action text can be split onto multiple lines, so use explicit
## newlines in translations to control where the line break appears (e.g., to
## avoid breaking quoted text).

onboarding-button-label-try-now = ລອງເລີຍຕອນນີ້
onboarding-button-label-get-started = ເລີ່ມ​ຕົ້ນ

## Welcome modal dialog strings

onboarding-welcome-header = ຍິນດີຕ້ອນຮັບສູ່ { -brand-short-name }
onboarding-start-browsing-button-label = ເລີ່ມການທ່ອງເວັບ
onboarding-cards-dismiss =
    .title = ຍົກເລີກ
    .aria-label = ຍົກເລີກ

## Firefox Sync modal dialog strings.

onboarding-sync-welcome-header = ເອົາ { -brand-product-name } ໄປກັບທ່ານ
onboarding-sync-welcome-content = ຮັບບຸກມາກ, ປະຫວັດການໃຊ້ງານ, ລະຫັດຜ່ານ ແລະການຕັ້ງຄ່າອື່ນໆໃນທຸກໆອຸປະກອນຂອງທ່ານ.
onboarding-sync-welcome-learn-more-link = ຮຽນຮູ້ເພີ່ມເຕີມກ່ຽວກັບບັນຊີ Firefox
onboarding-sync-form-invalid-input = ຕ້ອງການອີເມວທີ່ຖືກຕ້ອງ
onboarding-sync-legal-notice = ໂດຍການດຳເນີນການຕໍ່, ຖືວ່າທ່ານຍອມຮັບ<a data-l10n-name="terms">ເງື່ອນໄຂການໃຫ້ບໍລິການ</a>ແລະ<a data-l10n-name="privacy">ນະໂຍບາຍຄວາມເປັນສ່ວນຕົວ</a>.
onboarding-sync-form-input =
    .placeholder = ອີເມລ
onboarding-sync-form-continue-button = ສືບຕໍ່
onboarding-sync-form-skip-login-button = ຂ້າມຂັ້ນຕອນນີ້

## This is part of the line "Enter your email to continue to Firefox Sync"

onboarding-sync-form-header = ປ້ອນທີ່ຢູ່ອີເມລຂອງທ່ານ
onboarding-sync-form-sub-header = ເພື່ອດຳເນີນການຕໍ່ໄປຍັງ { -sync-brand-name }

## These are individual benefit messages shown with an image, title and
## description.


## These strings belong to the individual onboarding messages.


## Each message has a title and a description of what the browser feature is.
## Each message also has an associated button for the user to try the feature.
## The string for the button is found above, in the UI strings section

onboarding-private-browsing-title = ການທ່ອງເວັບແບບສ່ວນຕົວ
onboarding-private-browsing-text = ທ່ອງເວັບດ້ວຍຕົວທ່ານເອງ. ການທ່ອງເວັບແບບສ່ວນຕົວພ້ອມກັບການປິດກັ້ນເນື້ອຫາລະງັບໂຕຕິດຕາມອອນໄລທີ່ຕິດຕາມທ່ານໃນເວັບຕ່າງໆ.
onboarding-screenshots-title = ຖ່າຍພາບໜ້າຈໍ
onboarding-screenshots-text = ຈັບ, ບັນທຶກ ແລະ ແບ່ງປັນພາບຖ່າຍໜ້າຈໍ - ໂດຍບໍ່ຕ້ອງອອກຈາກ { -brand-short-name }. ຈັບພາບເປັນບໍລິເວນ ຫຼື ທັງໜ້າຕາມທີ່ທ່ານທ່ອງເວັບ. ຈາກນັ້ນບັນທຶກໄປຍັງເວັບເພື່ອຄວາມງ່າຍໃນການເຂົ້າເຖິງ ແລະ ການແບ່ງປັນ.
onboarding-addons-title = ສ່ວນເສີມ
onboarding-addons-text = ເພີ່ມຄຸນລັກສະນະເພີມເຕີມທີ່ເຮັດໃຫ້ { -brand-short-name } ເຮັດວຽກໄດ້ຫຼາຍຂື້ນສຳລັບທ່ານ. ປຽບທຽບລາຄາ, ກວດສອບສະພາບອາກາດ ຫຼື ສະແດງບຸກຄະລິກກະພາບຂອງທ່ານດ້ວຍຊຸດຕົກແຕ່ງທີ່ກຳນົດເອງ.
onboarding-ghostery-title = Ghostery
onboarding-ghostery-text = ຊອກຫາໄວຂື້ນ, ສະຫລາດຂື້ນ ຫຼື ປອດໄພກວ່າໂດຍ extension  ແບບ Ghostery ເຊິ່ງເຮັດໃຫ້ທ່ານສາມາດບັອກການໂຄສະນາທີ່ຫນ້າລຳຄານໄດ້.
# Note: "Sync" in this case is a generic verb, as in "to synchronize"
onboarding-fxa-title = Sync

## Message strings belonging to the Return to AMO flow

