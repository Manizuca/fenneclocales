<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY % brandDTD
    SYSTEM "chrome://branding/locale/brand.dtd">
  %brandDTD;

<!-- These strings are used by Firefox's custom about:certerror page,
a replacement for the standard security certificate errors produced
by NSS/PSM via netError.xhtml. -->

<!ENTITY certerror.pagetitle  "ການເຊື່ອມຕໍ່ທີ່ບໍ່ຫນ້າເຊື່ອຖືໄດ້">
<!ENTITY certerror.longpagetitle "ການເຊື່ອມຕໍ່ນີ້ແມ່ນບໍ່ມີຄວາມຫນ້າເຊື່ອຖືໄດ້">

<!-- Localization note (certerror.introPara1) - The string "#1" will
be replaced at runtime with the name of the server to which the user
was trying to connect. -->
<!ENTITY certerror.introPara1 "ທ່ານໄດ້ຮ້ອງຂໍ &brandShortName; ເພື່ອເຊື່ອມຕໍ່ກັບ ຢ່າງປອດໄພ <b>#1</b>, ແຕ່ວ່າພວກເຮົາບໍ່ສາມາດຢັ້ງຢືນວ່າການເຊື່ອມຕໍ່ຂອງທ່ານແມ່ນປອດໄພ.">

<!ENTITY certerror.whatShouldIDo.heading "ຂ້ອຍຄວນຈະເຮັດແນວໃດ?">
<!ENTITY certerror.whatShouldIDo.content "ຖ້າວ່າຕາມປົກກະຕິແລ້ວທ່ານເຊື່ອມຕໍ່ກັບ ເວັບໄຊທ໌ນີ້ໄດ້ໂດຍບໍ່ມີບັນຫາ, ຄວາມຜິດພາດນີ້ອາດເປັນໄປໄດ້ວ່າມີຜູ້ໃດຜູ້ຫນຶ່ງ ກຳລັງພະຍາຍາມທີ່ຈະປອມເວັບໄຊທ໌ນີ້ຂື້ນມາ ແລະ ທ່ານບໍ່ຄວນສືບຕໍ່ເຂົ້າໄປເວັບໄຊທ໌ນີ້.">
<!ENTITY certerror.getMeOutOfHere.label "ພາຂ້ອຍອອກຈາກບ່ອນນີ້!">

<!ENTITY certerror.expert.heading "ຂ້າພະເຈົ້າເຂົ້າໃຈໃນຄວາມສ່ຽງນີ້">
<!ENTITY certerror.expert.content "ຖ້າຫາກທ່ານເຂົ້າໃຈວ່າທ່ານເຮັດຫຍັງຢູ່, ທ່ານ ສາມາດບອກ &brandShortName; ໃຫ້ເຊື່ອຖືສິງລະບູຕົວຕົນຂອງເວັບໄຊທ໌ນີ້. <b>ຖ້າຫາກວ່າທ່ານໄວ້ໃຈເວັບໄຊທ໌ນີ້, ຂໍ້ຜິດພາດນີ້ອາດເກີດຂື້ນມາຈາກມີ ຄົນໃດຄົນຫນຶ່ງກຳລັງແຊກແຊງການເຊື່ອມຕໍ່ຂອງທ່ານຢູ່.</b>">
<!ENTITY certerror.expert.contentPara2 "ບໍ່ຄວນອະນຸຍາດໃຫ້ມີຂໍ້ຍົກເວັ້ນຫາກ ທ່ານບໍ່ມີເຫດຜົນທີ່ດີພໍວ່າເປັນຫຍັງເວັບໄຊທ໌ນີ້ຈຶ່ງບໍ່ມີການຢືນຢັນໂຕທີ່ຖືກຕ້ອງ.">
<!ENTITY certerror.addTemporaryException.label "ເຂົ້າໄປເບິງເວັບໄຊທ໌">
<!ENTITY certerror.addPermanentException.label "ເພີ່ມຂໍ້ຍົກເວັ້ນຢ່າງຖາວອນ">

<!ENTITY certerror.technical.heading "ລາຍລະອຽດທາງດ້ານເຕັກນິກ">
