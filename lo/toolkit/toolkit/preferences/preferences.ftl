# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

password-not-set =
    .value = (ຍັງບໍ່ໄດ້ຕັ້ງ)
failed-pw-change = ບໍ່ສາມາດປ່ຽນລະຫັດຜ່ານຫລັກໄດ້.
incorrect-pw = ທ່ານໄດ້ໃສ່ລະຫັດຜ່ານຫລັກບໍ່ຖືກຕ້ອງ. ກະລຸນາລອງໃຫມ່ອີກຄັ້ງ.
pw-change-ok = ປ່ຽນລະຫັດຫລັກຜ່ານສຳເລັດແລ້ວ.
pw-empty-warning = ທ່ານໄດ້ເກັບລະຫັດຜ່ານຂອງເວັບ ແລະ ອີເມລເອົາໄວ້, ຟອມຂໍ້ມູນ ແລະ keys ສ່ວນຕົວຈະບໍ່ໄດ້ຮັບການປ້ອງກັນ.
pw-erased-ok = ທ່ານໄດ້ລົບລະຫັດຜ່ານຫລັກຂອງທ່ານອອກແລ້ວ. { pw-empty-warning }
pw-not-wanted = ຄຳເຕືອນ! ທ່ານໄດ້ຕັດສິນໃຈທີ່ຈະບໍ່ນຳໃຊ້ລະຫັດຜ່ານຫລັກ. { pw-empty-warning }
pw-change2empty-in-fips-mode = ຕອນນີ້ທ່ານກຳຫລັງຢູ່ໃນໂຫມດ FIPS. FIPS ຕ້ອງການໃຊ້ລະຫັດຜ່ານຫລັກ.
pw-change-success-title = ປ່ຽນລະຫັດຜ່ານສຳເລັດ
pw-change-failed-title = ປ່ຽນລະຫັດຜ່ານບໍ່ສຳເລັດ
pw-remove-button =
    .label = ລຶບ
set-password =
    .title = ປ່ຽນລະຫັດຂັ້ນສູງ
set-password-old-password = ລະຫັດຜ່ານປະຈຸບັນ:
set-password-new-password = ໃສ່ລະຫັດຜ່ານໃຫມ່
set-password-reenter-password = ໃສ່ລະຫັດຜ່ານໃຫມ່ອີກຄັ້ງ:
set-password-meter = ຕົວວັດແທກຄຸນນະພາບລະຫັດຜ່ານ
set-password-meter-loading = ກຳລັງໂຫລດ
master-password-description = ລະຫັດຜ່ານຕົ້ນສະບັບຖືກນໍາໃຊ້ເພື່ອປົກປ້ອງຂໍ້ມູນທີ່ລະອຽດອ່ອນເຊັ່ນລະຫັດຜ່ານຂອງເວັບໄຊທ໌. ຖ້າທ່ານສ້າງລະຫັດຜ່ານຕົ້ນສະບັບທ່ານຈະຖືກຮ້ອງຂໍໃຫ້ເຂົ້າມັນຄັ້ງຫນຶ່ງຕໍ່ເວລາເມື່ອ { -brand-short-name } ລວບລວມຂໍ້ມູນທີ່ຖືກບັນທຶກໄວ້ໂດຍການລະຫັດຜ່ານ.
master-password-warning = ກະລຸນາໃຫ້ແນ່ໃຈວ່າທ່ານຈື່ລະຫັດຜ່ານຕົ້ນສະບັບທີ່ທ່ານຕັ້ງໄວ້. ຖ້າທ່ານລືມລະຫັດຜ່ານຕົ້ນສະບັບຂອງທ່ານ, ທ່ານຈະບໍ່ສາມາດເຂົ້າເຖິງຂໍ້ມູນໃດໆທີ່ໄດ້ຮັບການປ້ອງກັນໂດຍມັນ.
remove-password =
    .title = ລຶບລະຫັດຂັ້ນສູງ
remove-info =
    .value = ທ່ານຕ້ອງໃສ່ລະຫັດຜ່ານປະຈຸບັນຂອງທ່ານ ເພື່ອດຳເນີນການ:
remove-warning1 = ລະຫັດຜ່ານຂັ້ນສູງຂອງທ່ານແມ່ນໃຊ້ເພື່ອປ້ອງກັນຂໍ້ມູນທີ່ລະອຽດ ຄ້າຍຄືລະຫັດຜ່ານເວັບໄຊຕ໌
remove-warning2 = ຖ້າຫາກທ່ານລຶບລະຫັດຜ່ານຂັ້ນສູງຂອງທ່ານ ຂໍ້ມູນຂອງທ່ານຈະບໍ່ໄດ້ການປ້ອງກັນ ຖ້າຄອມພິວເຕີຂອງທ່ານຖືກບຸກລຸກ
remove-password-old-password =
    .value = ລະຫັດຜ່ານປະຈຸບັນ
