# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = ກ່ຽວກັບເຄື່ອຂາຍ
warning = ນີ້ແມ່ນຢູ່ໃນຊ່ວງທົດລອງນຳໃຊ້. ກະລຸນານຳໃຊ້ພາຍໃຕ້ການດູແລຂອງຜູ້ໃຫຍ່.
show-next-time-checkbox = ສະແດງຄຳເຕືອນນີ້ໃນຄັ້ງຕໍ່ໄປ
ok = OK
http = HTTP
sockets = Sockets
dns = DNS
websockets = WebSockets
refresh = ຟື້ນຟູ
auto-refresh = ຟື້ນຟູຕົວເອງໃນທຸກໆ 3 ວິນາທີ
hostname = Hostname
port = ພັອດ
ssl = SSL
active = ​ເປີດໃຊ້ງານ
idle = ວ່າງ
host = Host
tcp = TCP
sent = ສົ່ງ
received = ໄດ້ຮັບ
family = ຄອບຄົວ
addresses = ທີ່ຢູ່​
expires = ຫມົດອາຍຸ (ວິນາທີ)
messages-sent = ສົ່ງຂໍ້ຄວາມ
messages-received = ໄດ້ຮັບຂໍ້ຄວາມ
bytes-sent = ສົ່ງ Bytes
bytes-received = ໄດ້ຮັບ Bytes
logging = ກຳລັງບັນທຶກລັອກ
log-tutorial = ເບິງ <a data-l10n-name="logging">ການລັອກ HTTP</a> ສຳລັບວິທີການນຳໃຊ້ເຄື່ອງມືນີ້.
current-log-file = ລັອກໄຟລ໌ຕອນນີ້:
current-log-modules = ໂມດູລລັອກຕອນນີ້:
set-log-file = ຕັ້ງລັອກໄຟລ໌
set-log-modules = ຕັ້ງໂມດູລລັອກ
start-logging = ເລີ່ມການບັນທຶກລັອກ
stop-logging = ຢຸດການບັນທຶກລັອກ
dns-lookup = DNS Lookup
dns-lookup-button = Resolve
dns-domain = ໂດເມນ:
dns-lookup-table-column = IPs
