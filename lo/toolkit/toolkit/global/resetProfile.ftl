# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

refresh-profile-dialog =
    .title = ຟື້ນຟູ { -brand-short-name }
refresh-profile-dialog-button =
    .label = ຟື້ນຟູ { -brand-short-name }
refresh-profile-description = ເລີ່ມໃຫມ່ເພື່ອແກ້ໄຂບັນຫາ ແລະ ກູ້ຄືນປະສິດພາບການເຮັດວຽກ.
refresh-profile-description-details = ສິ່ງນີ້ຈະ:
refresh-profile-remove = ລຶບ add-ons ແລະ ການປັບແຕ່ງຂອງທ່ານອອກໄປ
refresh-profile-restore = ກູ້ຄືນການຕັ້ງຄ່າບຣາວເຊີຂອງທ່ານກັບໄປສູ່ຄ່າເດີມຂອງມັນ
refresh-profile = ປັບແຕ່ງ { -brand-short-name } ໃຫ້ດີຂື້ນກ່ວາເກົ່າ
refresh-profile-button = ຟື້ນຟູ { -brand-short-name }
