# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (document_title, cannot_retrieve_log):
# The text "WebRTC" is a proper noun and should not be translated.
# It is the general label for the standards based technology. see http://www.webrtc.org
document_title = WebRTC ພາຍໃນ
cannot_retrieve_log = ບໍ່ສາມາດເອີ້ນເອົາຂໍ້ມູນ log ຂອງ WebRTC

# LOCALIZATION NOTE (save_page_msg):
# %1$S will be replaced by a full path file name: the target of the SavePage operation.
save_page_msg = ໄດ້ບັນທຶກຫນ້າໄປໄວ້ທີ່: %1$S

# LOCALIZATION NOTE (save_page_dialog_title): "about:webrtc" is a internal browser URL and should not be
# translated. This string is used as a title for a file save dialog box.
save_page_dialog_title = ບັນທຶກ about:webrtc ທີ່

# LOCALIZATION NOTE (debug_mode_off_state_msg):
# %1$S will be replaced by the full path file name of the debug log.
debug_mode_off_state_msg = Log ຂອງການຕິດຕາມສາມາດພົບໄດ້ທີ່: %1$S

# LOCALIZATION NOTE (debug_mode_on_state_msg):
# %1$S will be replaced by the full path file name of the debug log.
debug_mode_on_state_msg = ເປີດໃຊ້ງານໂຫມດດີບັກ, Log ຂອງການຕິດຕາມທີ່: %1$S

# LOCALIZATION NOTE (aec_logging_msg_label, aec_logging_off_state_label,
# aec_logging_on_state_label, aec_logging_on_state_msg):
# AEC is an abbreviation for Acoustic Echo Cancellation.
aec_logging_msg_label = AEC Logging
aec_logging_off_state_label = ເລີ່ມການ log ສຳລັບ AEC
aec_logging_on_state_label = ຢຸດການ log ສຳລັບ AEC
aec_logging_on_state_msg = ເປີດນຳໃຊ້ການ log ສຳລັບ AEC (ສືສານກັບ caller ຈັກສອງສາມນາທີ ຫລັງຈາກນັ້ນໃຫ້ຢຸດການແຄັບເຈີ)

# LOCALIZATION NOTE (aec_logging_off_state_msg):
# %1$S will be replaced by the full path to the directory containing the captured log files.
# AEC is an abbreviation for Acoustic Echo Cancellation.
aec_logging_off_state_msg = ແຄັບເຈີຂອງ log ໄຟລ໌ສາມາດພົບໄດ້ໃນ: %1$S

# LOCALIZATION NOTE (peer_connection_id_label): "PeerConnection" is a proper noun
# associated with the WebRTC module. "ID" is an abbreviation for Identifier. This string
# should not normally be translated and is used as a data label.
peer_connection_id_label = PeerConnection ID

# LOCALIZATION NOTE (sdp_heading, local_sdp_heading, remote_sdp_heading):
# "SDP" is an abbreviation for Session Description Protocol, an IETF standard.
# See http://wikipedia.org/wiki/Session_Description_Protocol
sdp_heading = SDP
local_sdp_heading = SDP ໃນເຄື່ອງ
remote_sdp_heading = SDP ໄລຍະໄກ

# LOCALIZATION NOTE (offer, answer):
# offer and answer describe whether the local sdp is an offer or answer or
# the remote sdp is an offer or answer.  These are appended to the local and
# remote sdp headings.

# LOCALIZATION NOTE (rtp_stats_heading): "RTP" is an abbreviation for the
# Real-time Transport Protocol, an IETF specification, and should not
# normally be translated. "Stats" is an abbreviation for Statistics.
rtp_stats_heading = ສະຖິຕິຂອງ RTP

# LOCALIZATION NOTE (ice_state, ice_stats_heading): "ICE" is an abbreviation
# for Interactive Connectivity Establishment, which is an IETF protocol,
# and should not normally be translated. "Stats" is an abbreviation for
# Statistics.
ice_state = ສະຖານະຂອງ ICE
ice_stats_heading = ສະຖິຕິຂອງ ICE
ice_restart_count_label = ເລີ່ມເຮັດວຽກ ICE ໃຫມ່
ice_rollback_count_label = ຍ້ອນກັບ ICE
ice_pair_bytes_sent = ໄບທ໌ທີ່ສົ່ງແລ້ວ
ice_pair_bytes_received = ໄບທ໌ທີ່ໄດ້ຮັບ


# LOCALIZATION NOTE (av_sync_label): "A/V" stands for Audio/Video.
# "sync" is an abbreviation for sychronization. This is used as
# a data label.
av_sync_label = A/V sync

# LOCALIZATION NOTE (jitter_buffer_delay_label): A jitter buffer is an
# element in the processing chain, see http://wikipedia.org/wiki/Jitter
# This is used as a data label.
jitter_buffer_delay_label = ຄວາມລ່າຊ້າຂອງ Jitter-buffer

# LOCALIZATION NOTE (avg_bitrate_label, avg_framerate_label): "Avg." is an abbreviation
# for Average. These are used as data labels.
avg_bitrate_label = ຄ່າສະເລ່ຍຂອງ bitrate
avg_framerate_label = ຄ່າສະເລ່ຍຂອງ framerate

# LOCALIZATION NOTE (typeLocal, typeRemote): These adjectives are used to label a
# line of statistics collected for a peer connection. The data represents
# either the local or remote end of the connection.
typeLocal = ໃນເຄື່ອງ
typeRemote = ໄລຍະໄກ

# LOCALIZATION NOTE (nominated): This adjective is used to label a table column.
# Cells in this column contain the localized javascript string representation of "true"
# or are left blank.
nominated = ສະເຫນີຊື່ແລ້ວ

# LOCALIZATION NOTE (selected): This adjective is used to label a table column.
# Cells in this column contain the localized javascript string representation of "true"
# or are left blank. This represents an attribute of an ICE candidate.
selected = ເລືອກແລ້ວ

# LOCALIZATION NOTE (trickle_caption_msg2, trickle_highlight_color_name2): ICE
# candidates arriving after the remote answer arrives are considered trickled
# (an attribute of an ICE candidate). These are highlighted in the ICE stats
# table with light blue background. %S is replaced by
# trickle_highlight_color_name2 ("blue"), highlighted with a light blue
# background to visually match the trickled ICE candidates.

save_page_label = ບັນທຶກຫນ້ານີ້
debug_mode_msg_label = ໂຫມດດີບັກ
debug_mode_off_state_label = ເລີ່ມໂຫມດດີບັກ
debug_mode_on_state_label = ຢຸດໂຫມດດີບັກ
stats_heading = ສະຖິຕິຂອງແຊສຊັນນີ້
stats_clear = ລ້າງປະຫວັດ
log_heading = Log ຂອງການເຊື່ອມຕໍ່ນີ້ 
log_clear = ລຶບລັອກ
log_show_msg = ສະແດງ log
log_hide_msg = ເຊື່ອງ log
connection_closed = ປິດແລ້ວ
local_candidate = Local Candidate
remote_candidate = Remote Candidate
priority = ຄວາມສຳຄັນ
fold_show_msg = ສະແດງລາຍລະອຽດ
fold_show_hint = ຄລິກເພື່ອຂະຫຍາຍພາກສ່ວນນີ້
fold_hide_msg = ເຊື່ອງລາຍລະອຽດ
fold_hide_hint = ຄລິກເພື່ອຢໍ້ພາກສ່ວນນີ້
dropped_frames_label = ດລັອບເຟຣມນີ້ແລ້ວ
discarded_packets_label = ຖີ້ມເຟຣມນີ້ໄປ
decoder_label = ຕົວຖອດລະຫັດ
encoder_label = ຕົວເຂົ້າລະຫັດ
received_label = ໄດ້ຮັບແລ້ວ
packets = ແພັກເກັດ
lost_label = ສູນຫາຍ
jitter_label = Jitter
sent_label = ສົ່ງ

