# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

fxa-pair-device-dialog =
    .title = മറ്റൊരു ഉപകരണം ബന്ധിപ്പിക്കുക
    .style = width: 26em; min-height: 35em;
fxa-qrcode-heading-phase2 = 2. എന്നിട്ട് { -sync-brand-short-name }ലെക് പ്രവേശിക്കുക, അല്ലെങ്കിൽ ആൻഡ്രോയിഡിൽ  { -sync-brand-short-name } ക്രമീകരണങ്ങൾക്കുള്ളിൽ നിന്ന് ജോടിയാക്കൽ കോഡ് സ്കാൻ ചെയ്യുക.
fxa-qrcode-error-title = ജോടിയാക്കൽ പരാജയപ്പെട്ടു.
fxa-qrcode-error-body = വീണ്ടും ശ്രമിയ്ക്കുക
