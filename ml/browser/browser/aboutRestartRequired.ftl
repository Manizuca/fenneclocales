# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = പുനഃരാരംഭിക്കേണ്ടത് ആവശ്യമാണ്
restart-required-header = ക്ഷമിക്കണം.ഒരു ചെറിയ കാര്യം ചെയ്യണം നിങ്ങൾക്ക് തുടരാൻ.
restart-button-label = { -brand-short-name } പുനരാരംഭിക്കുക
