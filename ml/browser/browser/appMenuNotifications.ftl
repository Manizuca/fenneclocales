# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = പുതിയ { -brand-shorter-name } പുതുക്കല്‍ ലഭ്യമാണ്.
    .buttonlabel = പുതുക്കല്‍ ഡൗൺലോഡു
    .buttonaccesskey = D
    .secondarybuttonlabel = ഇപ്പോൾ വേണ്ട
    .secondarybuttonaccesskey = N
appmenu-update-available-message = മികച്ച വേഗതയ്ക്കും സ്വകാര്യതക്കും നിങ്ങളുടെ { -brand-shorter-name } അപ്ഡേറ്റ് ചെയ്യുക.
appmenu-update-manual =
    .label = { -brand-shorter-name } നു പുതിയ പതിപ്പിലേക്ക് പുതുക്കാന്‍ പറ്റില്ല.
    .buttonlabel = { -brand-shorter-name } ഡൌൺലോഡ് ചെയ്യുക
    .buttonaccesskey = D
    .secondarybuttonlabel = ഇപ്പോൾ വേണ്ട
    .secondarybuttonaccesskey = N
appmenu-update-manual-message = { -brand-shorter-name } ന്റെ പുതിയ പകർപ്പ് ഡൌൺലോഡ് ചെയ്യുക. അത് ഇൻസ്റ്റാൾ ചെയ്യാൻ ഞങ്ങൾ നിങ്ങളെ സഹായിക്കാം.
appmenu-update-whats-new =
    .value = പുതിയതായി എന്താണ് ഉള്ളതെന്ന് കാണുക.
appmenu-update-unsupported =
    .label = { -brand-shorter-name } പുതുക്കാന്‍ കഴിഞ്ഞില്ല.
    .buttonlabel = കൂടുതല്‍ അറിയുക
    .buttonaccesskey = L
    .secondarybuttonlabel = അടയ്ക്കുക
    .secondarybuttonaccesskey = C
appmenu-update-unsupported-message = നിങ്ങളുടെ ഉപകരണത്തിൽ { -brand-shorter-name } ന്റെ ഏറ്റവും പുതിയ പതിപ്പ് പിന്തുണയ്ക്കുന്നില്ല.
appmenu-update-restart =
    .label = { -brand-shorter-name } അപ്ഡേറ്റ് ചെയ്യുവാനായി റീസ്റ്റാർട്ട് ചെയ്യുക.
    .buttonlabel = റീ സ്റ്റാർട്ട് ചെയ്ത് പുനഃസ്ഥാപിക്കുക
    .buttonaccesskey = R
    .secondarybuttonlabel = ഇപ്പോൾ വേണ്ട
    .secondarybuttonaccesskey = N
appmenu-update-restart-message = റീസ്റ്റാർട്ടിങ്ങു് ശേഷം { -brand-shorter-name } സ്വകാര്യ ബ്രൌസിംഗ് മോഡിൽ ഇല്ലാത്ത നിങ്ങളുടെ എല്ലാ ഓപ്പൺ ടാബുകളും വിൻഡോകളും പുനഃസ്ഥാപിക്കും.
appmenu-addon-private-browsing-installed =
    .buttonlabel = ശരി, മനസ്സിലായി
    .buttonaccesskey = O
appmenu-addon-post-install-message = നിങ്ങളുടെ ആഡ്-ഓണുകൾ കൈകാര്യം ചെയ്യാന്‍ <Image data-l10n-name = 'addon-menu-icon-> </ image> മെനുവിൽ <image data-l10n-name =' addon-install-icon-> </ image> -ൽ ക്ലിക്കുചെയ്യുക,
appmenu-addon-post-install-incognito-checkbox =
    .label = സ്വകാര്യ വിൻഡോസിൽ പ്രവർത്തിക്കുന്നതിന് ഈ എക്സ്റ്റന്‍ഷനെ അനുവദിക്കുക
    .accesskey = A
appmenu-addon-private-browsing =
    .label = സ്വകാര്യ വിൻഡോകളിൽ വിപുലീകരണങ്ങളിലേക്ക് മാറ്റുക
    .buttonlabel = വിപുലീതരണം കൈകാര്യം ചെയ്യുക
    .buttonaccesskey = M
    .secondarybuttonlabel = ശരി, മനസിലായി
    .secondarybuttonaccesskey = o
appmenu-addon-private-browsing-message = നിങ്ങൾ ക്രമീകരണങ്ങളിൽ ഇത് അനുവദിച്ചില്ലെങ്കിൽ { -brand-shorter-name }ൽ ചേർക്കുന്ന പുതിയ വിപുലീകരണങ്ങൾ സ്വകാര്യ ജാലകത്തിൽ  പ്രവർത്തിക്കില്ല.
appmenu-addon-private-browsing-learn-more = വിപുലീകരണ ക്രമീകരണങ്ങൾ എങ്ങനെ നിയന്ത്രിക്കാമെന്ന് മനസിലാക്കുക
