# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = Daj nikó dugui' raj sun red
warning = Nan ni sa raj hue' huin ma. Da'ui ngo sí achij ga'ni' si suun.
show-next-time-checkbox = Diganj niko' huaj sa hua ahii.
ok = Garayina
http = HTTP
sockets = Sockets
dns = DNS
websockets = WebSockets
refresh = Nagui'iaj nako'
auto-refresh = Nagui'iaj nako' da' 3 diu li
hostname = Si yugui sí raj sun
port = Puerto
http2 = HTTP/2
ssl = SSL
active = Ngà' 'iaj sunj
idle = Nitaj si 'iaj sunj
host = Sa nikaj ñu'unj
tcp = TCP
sent = Ga'nìnj gan'an
received = Nahuin ra'á
family = Dugui'
trr = TRR
addresses = Hiuj gun'
expires = Gire'ej (Segundos)
messages-sent = Nga ga'anj nuguan'an
messages-received = Nuguan nahuin ro'o'
bytes-sent = Bytes sa ga'nin'
bytes-received = Bytes nahuin ro'o'
logging = Nej riña ayi'ì sesión
log-tutorial = Ni'iaj riña na <a data-l10n-name="logging">HTTP Logging</a> da' guini'in daj garasunt ma.
current-log-file = Riña Ma sa Nakaa
current-log-modules = Riña Ma sa Naka doj:
set-log-file = Set Log File
set-log-modules = Nuto' riña Nachra So':
start-logging = Gayi'i' Nachro'
stop-logging = Dunikin' sa Nachro'
dns-lookup = Nana'ui' DNS
dns-lookup-button = Nagi'io'
dns-domain = Nikaj ñu'unj:
dns-lookup-table-column = IPs
rcwn = RCWN giriñu
rcwn-status = RCWN giriñu
rcwn-cache-won-count = Cache gi'iaj gana guenda na
rcwn-net-won-count = Gi'iaj ganaj guênda
total-network-requests = Daran' sa achín red na
rcwn-operation = Sa 'iaj Kachê
rcwn-perf-open = Na'nïn
rcwn-perf-read = Gahia
rcwn-perf-write = Gachrun
rcwn-perf-entry-open = Hua ni'nïn rahue'e
rcwn-avg-short = Mediâ ru'u
rcwn-avg-long = Mediâ ye'èe
rcwn-std-dev-long = Desbiasiûn hua niña ye'èe
rcwn-cache-slow = Sa nahia naj kachê
rcwn-cache-not-slow = Sa nitaj si nahia naj kachê
