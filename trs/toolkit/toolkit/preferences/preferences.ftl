# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

password-not-set =
    .value = (nitāj si nunj)
failed-pw-change = Nitāj si huaj nadunā da’nga’ huì a’nïn’ïn.
incorrect-pw = Nu gachrūnt da’nga’ huì a’nïn’ huā hue’ê. Nachrūn ñun.
pw-change-ok = Ngà nadunâ da’nga’ huì a’nïn’ïn.
pw-empty-warning = Nej da’nga’ huì arâj sunt ngà nej si korreôt nun sà’ ‘iát, nej si datôt nī a’ngô nej da’nga’ huì arâj sunt si gā arán gīnun nej man.
pw-erased-ok = Nadurê’t da’nga’ huì a’nïn’ huā ‘iát. { pw-empty-warning }
pw-not-wanted = ¡Gā gudadû! Dadin’ nu gārasunt ‘ngō da’nga’ huì a’nïn’ïn. { pw-empty-warning }
pw-change2empty-in-fips-mode = Akuan’ nïn nī nunt ngà modô FIPS. FIPS nī ni’ñan ‘ngō Da’nga’ Huì a’nïn’ïn.
pw-change-success-title = Ngà gisîj ga’ue nadunât Da’nga’ Huìi
pw-change-failed-title = Nu ga’ue nādunaj Da’nga’ Huìi
pw-remove-button =
    .label = Guxūn
set-password =
    .title = Nadunā Da’nga’ Huì A’nïn’ïn
set-password-old-password = Da’nga’ huì nikājt akuan’ nïn:
set-password-new-password = Gachrūn da’nga’ huì nakàa:
set-password-reenter-password = Gachrūn ñû da’nga’ huìi:
set-password-meter = Sa ni’iāj si huā hue’ê da’nga’ huìi
set-password-meter-loading = Hìaj ayì'ij
master-password-description = Arâj sun’ ‘ngō da’nga’ huì a’nïn’ïn da’ dugumîn’ daran’ chre nej  nuguan’ huā ahī màn ‘iô’ dàj rû’ si danga’ huì sîtio. Si girīt ‘ngō da’nga’ huì a’nïn’ïn nī, nachìn aga’ nan da’ gachrūnt da’nga’ dan ngà gayi’ìt ‘ngō sesión nga { -brand-short-name } narì’ ñûnt nuguan’ nū sà’ dugumîn da’nga’ huì nan.
master-password-warning = Sasà’ ginun ruhuât si yugui ‘ngō da’nga’ huì gachrûnt.  Sisī gini’ñūnt man nī, si ga’ue gatūt gini’iājt nej nuguan’ màn duguminj.
remove-password =
    .title = Nadure’ da’nga’ huì a’nïn’ïn
remove-info =
    .value = Da’uît gachrūnt da’nga’ huì rajsunt akuan’ nïn da’ ga’ue gan’ānjt ne’ ñaan:
remove-warning1 = Da’nga’ huì a’nïn’ nikājt dan nī rugûñun’un da’ dugumînt nej nuguan’ huā ‘iát riña huājt.
remove-warning2 = Sisī guxūnt da’nga’ huì arajsunt, nī nuguan’ màn ‘iát nī se gimà huìj sisī huā ahí si agâ’t.
remove-password-old-password =
    .value = Da'nga' huì nikājt akuan' nïn:
