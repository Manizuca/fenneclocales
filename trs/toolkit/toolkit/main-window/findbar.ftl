# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### This file contains the entities needed to use the Find Bar.

findbar-next =
    .tooltiptext = Narì' sa tàj ñan riña yi'nïn' nuguan' dan
findbar-previous =
    .tooltiptext = Narì' sa nikò' rukù riña yi'nïn' nuguan' dan
findbar-find-button-close =
    .tooltiptext = Narán riña chrún achrûn' nuguan' nana'uî'
findbar-highlight-all =
    .label = Nagi'iaj rangà' daran'anj
    .accesskey = l
    .tooltiptext = Nagi'iaj rangà' daran' chre nej sa nikaj' dugui' ngà nuguan' dan
findbar-highlight-all2 =
    .label = Nagi'aj rangà' daran'anj
    .accesskey =
        { PLATFORM() ->
            [macos] l
           *[other] a
        }
    .tooltiptext = Nadigân ña ahui nej frâse
findbar-case-sensitive =
    .label = Match Case
    .accesskey = C
    .tooltiptext = Nana'uì' ngà nej mayûskula
findbar-entire-word =
    .label = Nuguan' hua chre'e
    .accesskey = W
    .tooltiptext = Nana'uì' maninaj nuguan' hua chre'e
