# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

sync-disconnect-dialog =
    .title = Gahui' { -sync-brand-short-name }
    .style = width: 36em; min-height: 35em;
sync-disconnect-heading = Dure' nej sa arajsun navegador riña aga' na anj. Antaj si naduret, ni si datos ni { -sync-brand-name } gimasa'aj riña si kuetat.
sync-disconnect-remove-sync-caption = Guxun' si dato { -sync-brand-name }
sync-disconnect-remove-sync-data = Markador, riña gaché nu', da'nga' huìi, etc.
sync-disconnect-remove-other-caption = Dure' nej dato huìi
sync-disconnect-remove-other-data = Kookie, kaché, dato sitio, etc.
# Shown while the disconnect is in progress
sync-disconnect-disconnecting = Gahui riña internet...
sync-disconnect-cancel =
    .label = Duyichîn'
    .accesskey = C

## Disconnect confirm Button
##
## The 2 labels which may be shown on the single "Disconnect" button, depending
## on the state of the checkboxes.

sync-disconnect-confirm-disconnect-delete =
    .label = Guxun' nej si dure'
    .accesskey = D
sync-disconnect-confirm-disconnect =
    .label = Ma si gahui' riña internet
    .accesskey = D
