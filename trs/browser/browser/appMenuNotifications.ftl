# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

appmenu-update-available =
    .label = Hua 'ngo sa nakàa guenda { -brand-shorter-name }.
    .buttonlabel = Naduni' sa nagi'iaj nakàa
    .buttonaccesskey = D
    .secondarybuttonlabel = Si ga'ue akuan'ni
    .secondarybuttonaccesskey = N
appmenu-update-available-message = Nagi'iaj nakà { -brand-shorter-name }da' gi'iaj sun hiò ma.
appmenu-update-manual =
    .label = Si ga'ue nagi'iaj nakàt { -brand-shorter-name }.
    .buttonlabel = Naduninj { -brand-shorter-name }
    .buttonaccesskey = D
    .secondarybuttonlabel = Si ga'ue akuan'ni
    .secondarybuttonaccesskey = N
appmenu-update-manual-message = Naduninj 'ngò sa nakàa guenda { -brand-shorter-name }.
appmenu-update-whats-new =
    .value = Ni'iaj si ngà gurugui' a'ngô sa nakàa.
appmenu-update-restart =
    .label = Duna'àj ni nachrun ñûnt da' nahuin nàkaj { -brand-shorter-name }.
    .buttonlabel = Nayi'ì ñut nī nagi'iaj ñut
    .buttonaccesskey = R
    .secondarybuttonlabel = Si ga'ue akuan'ni
    .secondarybuttonaccesskey = N
appmenu-update-restart-message = 'Ngà nayi'ì ñun, { -brand-shorter-name } ni na'nin ñunj daran' rakïj ñanj nī nej ventana hua ni'nïnj.
appmenu-addon-post-install-message = Ganikaj ñu'u' komplementos 'ngà ga'ui' klik <image data-l10n-name='addon-install-icon'></image> riña <image data-l10n-name='addon-menu-icon'></image> menû.
