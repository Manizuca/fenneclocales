# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

restart-required-title = Da'ui nayi'ì ñunj
restart-required-header = Si gamma ruat. A'kuan'ni nayi'ì ñunj.
restart-required-intro = Hiaj ga'nïn ñûnj 'ngo sa nahuin nakà se sa ña'an nìko huaa. Ga'ui' klik riña nayi'ìt { -brand-short-name } da' nanun hue'ej.
restart-required-description = Nagi'iaj ñûnj daran' nej si pajinât, bentâna ni rakïj ñanj, da' ga'ue ga hue'ê doj gache nunt.
restart-button-label = Nayi'ì nakà { -brand-short-name }
