# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = Gachrun URL gi'iaj nahuin nakà aplikasiûn ruhuât.
policy-Authentication = Nagi'iaj hue'ê autentikasiûn ngà 'na'a riña ne sitiô ga'ue gunùkuaj nej man.
policy-BlockAboutAddons = Narán riña sa dugumîn nej sa ga'ue nutò' doj (about:addons).
policy-BlockAboutConfig = Narán riña ga'ue gatu' riña pajinâ about:config.
policy-BlockAboutProfiles = Narán guenda gatut riña pajinâ about:profiles
policy-BlockAboutSupport = Narán guendâ gatut riña pajinâ about:support
policy-Bookmarks = Giri da'nga' dunanû rûa riña 'na' si dukuànj da'nga'a, riña menû Da'nga'a asi riña 'ngo karpeta hua yitïnj niñaa.
policy-Certificates = Ga'ue asi si ga'ue garasunt sertifikadô hua chre'e. Màn si chrej Windows huin man akuan' nïn.
policy-CertificatesDescription = duguatûj sertifikâdo asi garasun sertifikadô ngà huaa.
policy-Cookies = Ga'nïn asi si ga'nïnjt riña sitiô da' ga'nïn kokies.
policy-DisableAppUpdate = Si ga'nïnjt nahuin nakà nabegadôr.
policy-DisableBuiltinPDFViewer = Duyichin' si sun PDF.js, sa ni'io' PDF nu riña { -brand-short-name }.
policy-DisableDeveloperTools = Narán riña gatut riña màn nej rasuun ri ràa.
policy-DisableFeedbackCommands = duyichin' si sun nej komandô da' si ga'nïn nuguan' asîj riña menû (Ga'nïnj nuguan' ni duyinga' nuguan' nee).
policy-DisableFirefoxAccounts = Duyichin' nej sun hua riña { -fxaccount-brand-name }, nda Sync nej.
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Duyichin' sun nari ñadu'ua Pantâya.
policy-DisableFirefoxStudies = Si ga'nïnjt riña { -brand-short-name } nadi'ñunj.
policy-DisableForgetButton = Si ga'nïnjt gi'iaj sun butûn Gini'ñun'.
policy-DisableFormHistory = Nu nigi'ijt sa nana'uî't ni sa gahuin riña formulârio.
policy-DisableMasterPasswordCreation = Si yangà' ni, si ga'ue girit 'ngo dan'ga' huì a'nïn'ïn.
policy-DisablePocket = Duyichin' sun guendâ na'nínj sà't nej pâjina riña Pocket.
policy-DisablePrivateBrowsing = Duyichin' si sun nabegadôr huìi.
policy-DisableProfileImport = Duyichin' si sun komandô nikaj menû guendâ duguachinj nuguan' riña a'ngô nabegadôr.
policy-DisableProfileRefresh = Duyichin' si sun sa Nagi'iaj nakà { -brand-short-name } riña pajinâ about:support.
policy-DisableSafeMode = Duyichin' sun guendâ gayi'ìt riña Sa dugumî sò'. Nuguan' huaa: tekla mayuskula guendâ gatu' riña Sa Dugumî ni ma riña Windows ga'ue giyìchinj.
policy-DisableSecurityBypass = Si ga'nïntj sisi hua da'aj nej duguî' ruhuâ nadure' nej nuguan' tna'uej rayi'ît.
policy-DisableSetDesktopBackground = Duyuchin' si komandô menû taj Dunaj ginu ñadu'ua riña eskritôrio.
policy-DisableSetAsDesktopBackground = Duyichin' si komandô menû taj Dunaj ginu man riña eskritôrio guendâ nej ñadu'ua.
policy-DisableSystemAddonUpdate = Si ga'nïnjt guendâ na'nïnj nabegadôr na asi nagi'iaj nàkaj ne sa nata'a.
policy-DisableTelemetry = Dunâ'aj Telemetrîa.
policy-DisplayBookmarksToolbar = Nadiganj dukuàn ma si rasuun nej sa arâj sun yitïn'.
policy-DisplayMenuBar = Nadiganj man'an dukuán 'na' menû.
policy-DNSOverHTTPS = Nagi'iaj DNS riña HTTPS.
policy-DontCheckDefaultBrowser = Duyichin' sa natsij nabegadôr hua niña riña gayi'ìj.
# “lock” means that the user won’t be able to change this setting
policy-EnableTrackingProtection = Nachrun asi duyichîn't Sa narán riña sa mà riña Web.
# A “locked” extension can’t be disabled or removed by the user. This policy
# takes 3 keys (“Install”, ”Uninstall”, ”Locked”), you can either keep them in
# English or translate them as verbs. See also:
# https://github.com/mozilla/policy-templates/blob/master/README.md#extensions-machine-only
policy-Extensions = Ga'nïnj, si ga'nïnt asi garánt riña nej sa nata'a. Riña taj Ga'nïnj arâj sun man URLs . Riña taj Nadure' asi Narán da'a man'an man si IDs man.
policy-FlashPlugin = Ga'nïn asi si ga'nïnjt gi'iaj sun sa nata' riña Flash.
policy-HardwareAcceleration = Si sê sa yangà'a huin, duyichin' sa nagi'iaj hìo hardware.
# “lock” means that the user won’t be able to change this setting
policy-Homepage = Nachrun asi narrán akuan' riña pajinâ gayi'ìt.
policy-InstallAddonsPermission = Ga'nïn riña da'aj pâjina gà'nïn sa nata'a.
policy-NoDefaultBookmarks = Duyichin' si sun sa ri markadôr daj rû' riña { -brand-short-name }, ni nej markadôr chruun (Nej sa ni'iaj nìkot doj). Nuguan' huaa: sisi asîj gachin girit perfîl sinïin ni ga'ue garasunt nan.
policy-OfferToSaveLogins = Dunahuij sa nagi'iát da' ga'nïn riña { -brand-short-name } dunanun ruhuaj ngà gayi'ìt gatut asi da'nga' huì màn sà' 'iát. Ga'ue nada'a sa yangà'a asi sa diga'ñun'unj un.
policy-OverrideFirstRunPage = Nadure' Pajinâ nayi'nïnj sinïïn. Dunâj gatsì nuguan' nan sisi ruhuât nadurê't pajinâ nayi'nïnj sinïïn.
policy-OverridePostUpdatePage = Nadure' pajinâ nagi'iaj nakà gahui rukû sa nahuin nakà nan. Dunâj gatsì nan sisi ruhuât duyichin't pajinâ ne' rukù sa nahuin nakàa.
policy-Permissions = Natsïj riña achín ni'iát rayi'î aga' nari', aga' uxun nanèe ni nej nuguan' rugûñun'unj un.
policy-PopupBlocking = Ga'nïn da' da'aj sîtio nadigân bentanâ narugui' man'an.
policy-Proxy = Naduna dàj nagi'iát riña proxy.
policy-RequestedLocales = Nachrun dukuànj nej lokâl gachîn aplikasiûn nan dàj huin ruhuât.
policy-SanitizeOnShutdown = Nadure' daran' chre nuguan' hua ngà gaché nunt ngà gisîj dunà'ajt.
policy-SearchBar = Nachrun sa ruhuât ga yitïnj riña dukuán na'na'uî' sa nana'uî'. Hua nïn hua diû guendâ nagi'iát.
policy-SearchEngines = Nagi'iaj dàj hua riña sa rugûñu'unj nana'uî'. Màn riña Extended Support Release (ESR) ga'ue naduno' sa ruhuô' nan.
# For more information, see https://developer.mozilla.org/en-US/docs/Mozilla/Projects/NSS/PKCS11/Module_Installation
policy-SecurityDevices = Ga'nïnj nej modulô PKCS #11.
# “format” refers to the format used for the value of this policy. See also:
# https://github.com/mozilla/policy-templates/blob/master/README.md#websitefilter-machine-only
policy-WebsiteFilter = Narán nej sitiô nga gisij gatujt riñanj. Ganatsij doj ni'iajt dàj hua rayi'î sun nan.
