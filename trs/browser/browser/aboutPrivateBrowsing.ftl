# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

about-private-browsing-learn-more = Gahuin chrunt doj rayi'î <a data-l10n-name="learn-more">Gachenu hùi'</a>.
about-private-browsing-info-visited = Nej ñanj ngà' ni'io'
privatebrowsingpage-open-private-window-label = Ga'nin riña 'ngo ventanahuìi
    .accesskey = h
about-private-browsing-info-notsaved = Nga aché nunt riña 'ngo ventana huìi, { -brand-short-name }<strong>Nu nanun sà'aj</strong>:
about-private-browsing-info-bookmarks = Sa raj sun doj
about-private-browsing-info-searches = Nej sa nanan'uî'
about-private-browsing-info-downloads = Naduninj
private-browsing-title = Gachenu hùi'
about-private-browsing-info-saved = { -brand-short-name }<strong>Nanïnj sà'aj</strong>si:
about-private-browsing-info-clipboard = guxun nuguan'an ni nachrunt a'ngo hiuj u
about-private-browsing-info-temporary-files = Ñanj nitaj si nu yitïnj ïn
about-private-browsing-info-cookies = cookies
tracking-protection-start-tour = Ni'iaj dà 'iaj sunj
about-private-browsing-note = Gachenu hùi'<strong>Nitaj hua huì nunt</strong>riña internet. Si aga't na ni ni'in riña aché nut
about-private-browsing-not-private = Se riña ventana hua huìi nut akuan' nï
content-blocking-title = Garrun' riña kontenido
content-blocking-description = Hua da'aj nej sîtio ni nikaj man sa naga'naj ga'ue ni'iaj sa 'iát nga aché nunt riña internet. Riña nej bentanâ huìi, sa narán riña nej sa hua nikaj { -brand-short-name } ni narán man'an man riña nej sa naga'naj nan.
