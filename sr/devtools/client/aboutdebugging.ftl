# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the about:debugging UI.


# Page Title strings


# Sidebar strings

# Text displayed in the about:debugging sidebar when USB devices discovery is enabled.
about-debugging-sidebar-usb-enabled = USB омогућен
# Text displayed in sidebar items for remote devices where a compatible browser (eg
# Firefox) has not been detected yet. Typically, Android phones connected via USB with
# USB debugging enabled, but where Firefox is not started.
about-debugging-sidebar-runtime-item-waiting-for-browser = Чекам прегледача...

# Setup Page strings

# Text of the button displayed in the USB section of the setup page when USB debugging is enabled.
about-debugging-setup-usb-disable-button = Онемогућите USB уређаје
# USB section of the Setup page (USB status)
about-debugging-setup-usb-status-enabled = Омогућено
about-debugging-setup-usb-status-disabled = Онемогућено

# Runtime Page strings


# Debug Targets strings

# Label text used for default state of details of message component.
about-debugging-message-details-label = Детаљи
