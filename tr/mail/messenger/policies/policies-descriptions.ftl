# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Thunderbird installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-3rdparty = WebExtension’ların chrome.storage.managed aracılığıyla ulaşabileceği ilkeleri ayarla.
policy-AppUpdateURL = Özel uygulama güncelleme URL’sini ayarla.
policy-Authentication = Destekleyen web siteleri için bütünleşik kimlik doğrulamasını yapılandır.
policy-DNSOverHTTPS = HTTP üzerinden DNS’i yapılandır.
policy-Proxy = Vekil sunucu ayarlarını yapılandır.
policy-SanitizeOnShutdown2 = Kapatırken gezinti verilerini temizle.
policy-SSLVersionMax = Maksimum SSL sürümünü ayarla.
policy-SSLVersionMin = Minimum SSL sürümünü ayarla.
policy-SupportMenu = Yardım menüsüne özel bir destek öğesi ekle.
