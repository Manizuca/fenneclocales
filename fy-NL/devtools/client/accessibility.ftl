# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Mear ynfo
accessibility-text-label-header = Tekstlabels en -namen

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Warskôging
accessibility-fail =
    .alt = Flater
accessibility-best-practices =
    .alt = Goede foarbylden

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Brûk it attribút <code>alt</code> om <div>area</div>-eleminten dy't it attribút <span>href</span> hawwe te labeljen. <a>Mear ynfo</a>
accessibility-text-label-issue-dialog = Dialogen soene labeld wêze moatte. <a>Mear ynfo</a>
accessibility-text-label-issue-document-title = Dokuminten moatte in <code>title</code> hawwe. <a>Mear ynfo</a>
accessibility-text-label-issue-embed = Ynsluten ynhâld moat labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-figure = Ofbyldingen mei opsjonele byskriften moatte labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-fieldset = <code>fieldset</code>-eleminten moatte labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-fieldset-legend = Brûk it elemint <code>legend</code> om <span>fieldset</span>-eleminten te labeljen. <a>Mear ynfo</a>
accessibility-text-label-issue-form = Formuliereleminten moatte labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-form-visible = Formuliereleminten moatte in sichtber tekstlabel hawwe. <a>Mear ynfo</a>
accessibility-text-label-issue-frame = <code>frame</code>-eleminten moatte labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-glyph = Brûk it attribút <code>alt</code> om <span>mglyph</span>-eleminten te labeljen. <a>Mear ynfo</a>
accessibility-text-label-issue-heading = Kopteksten moatte labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-heading-content = Kopteksten moatte sichtbere tekstynhâld hawwe. <a>Mear ynfo</a>
accessibility-text-label-issue-iframe = Brûk it attribút <code>title</code> om <span>iframe</span>-ynhâld te beskriuwen. <a>Mear ynfo</a>
accessibility-text-label-issue-image = Ynhâld mei ôfbyldingen moat labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-interactive = Ynteraktive eleminten moatte labele wurde. <a>Mear ynfo</a>
accessibility-text-label-issue-optgroup-label = Brûk it attribút <code>label</code> om <span>optgroup</span>-eleminten te labeljen. <a>Mear ynfo</a>
accessibility-text-label-issue-toolbar = Arkbalken moatte labele wurde as der mear as ien arkbalke is. <a>Mear ynfo</a>
