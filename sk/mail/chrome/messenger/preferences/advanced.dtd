<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "Všeobecné">
<!ENTITY dataChoicesTab.label    "Odosielanie údajov">
<!ENTITY itemUpdate.label        "Aktualizácie">
<!ENTITY itemNetworking.label    "Sieť a miesto na disku">
<!ENTITY itemCertificates.label  "Certifikáty">

<!-- General Settings -->
<!ENTITY enableGlodaSearch.label       "Povoliť globálne vyhľadávanie a indexovanie správ">
<!ENTITY enableGlodaSearch.accesskey   "o">
<!ENTITY dateTimeFormatting.label      "Formát dátumu a času">
<!ENTITY languageSelector.label        "Jazyk">
<!ENTITY allowHWAccel.label            "Použiť hardvérové urýchľovanie (ak je dostupné)">
<!ENTITY allowHWAccel.accesskey        "h">
<!ENTITY storeType.label               "Spôsob ukladania správ pre nové účty:">
<!ENTITY storeType.accesskey           "b">
<!ENTITY mboxStore2.label              "Samostatný súbor pre každý priečinok (mbox)">
<!ENTITY maildirStore.label            "Samostatný súbor pre každú správu (maildir)">

<!ENTITY scrolling.label               "Posúvanie obsahu">
<!ENTITY useAutoScroll.label           "Použiť automatický posun">
<!ENTITY useAutoScroll.accesskey       "a">
<!ENTITY useSmoothScrolling.label      "Použiť plynulý posun">
<!ENTITY useSmoothScrolling.accesskey  "n">

<!ENTITY systemIntegration.label       "Integrácia so systémom">
<!ENTITY alwaysCheckDefault.label      "Pri štarte kontrolovať, či je &brandShortName; predvoleným poštovým klientom">
<!ENTITY alwaysCheckDefault.accesskey  "k">
<!ENTITY searchIntegration.label       "Umožniť službe &searchIntegration.engineName; prehľadávať správy">
<!ENTITY searchIntegration.accesskey   "U">
<!ENTITY checkDefaultsNow.label        "Skontrolovať…">
<!ENTITY checkDefaultsNow.accesskey    "S">
<!ENTITY configEditDesc.label          "Pokročilé nastavenia">
<!ENTITY configEdit.label              "Editor nastavení…">
<!ENTITY configEdit.accesskey          "E">
<!ENTITY returnReceiptsInfo.label      "Zistiť ako &brandShortName; spracováva potvrdenia o prečítaní">
<!ENTITY showReturnReceipts.label      "Potvrdenia o prečítaní…">
<!ENTITY showReturnReceipts.accesskey  "P">

<!-- Data Choices -->
<!ENTITY telemetrySection.label          "Telemetria">
<!ENTITY telemetryDesc.label             "Zdieľa údaje o výkonnosti, využívaní funkcií, hardvérovej konfigurácii a prispôsobení e-mailového klienta so spoločnosťou &vendorShortName; s cieľom urobiť &brandShortName; lepším.">
<!ENTITY enableTelemetry.label           "Povoliť telemetriu">
<!ENTITY enableTelemetry.accesskey       "t">
<!ENTITY telemetryLearnMore.label        "Ďalšie informácie">

<!ENTITY crashReporterSection.label      "Správy o zlyhaní">
<!ENTITY crashReporterDesc.label         "&brandShortName; odosiela správy o zlyhaní s cieľom pomôcť spoločnosti &vendorShortName; urobiť e-mailový klient stabilnejším a bezpečnejším.">
<!ENTITY enableCrashReporter.label       "Povoliť správy o zlyhaní">
<!ENTITY enableCrashReporter.accesskey   "z">
<!ENTITY crashReporterLearnMore.label    "Ďalšie informácie">

<!-- Update -->
<!-- LOCALIZATION NOTE (updateApp.label):
  Strings from aboutDialog.dtd are displayed in this section of the preferences.
  Please check for possible accesskey conflicts.
-->
<!ENTITY updateApp2.label                "Aktualizácie aplikácie &brandShortName;">
<!-- LOCALIZATION NOTE (updateApp.version.*): updateApp.version.pre is
  followed by a version number, keep the trailing space or replace it with
  a different character as needed. updateApp.version.post is displayed after
  the version number, and is empty on purpose for English. You can use it
  if required by your language.
 -->
<!ENTITY updateApp.version.pre           "Verzia ">
<!ENTITY updateApp.version.post          "">
<!ENTITY updateAppAllow.description      "Povoliť aplikácii &brandShortName;">
<!ENTITY updateAuto.label                "Automaticky inštalovať aktualizácie (odporúčané z dôvodu zvýšenej bezpečnosti)">
<!ENTITY updateAuto.accesskey            "A">
<!ENTITY updateCheck.label               "Vyhľadávať aktualizácie, ale poskytnúť možnosť zvoliť, či sa nainštalujú">
<!ENTITY updateCheck.accesskey           "k">
<!ENTITY updateHistory.label             "Zobraziť históriu aktualizácií">
<!ENTITY updateHistory.accesskey         "Z">

<!ENTITY useService.label                "Na inštaláciu aktualizácií použiť službu na pozadí">
<!ENTITY useService.accesskey            "k">

<!ENTITY updateCrossUserSettingWarning.description "Toto nastavenie sa vzťahuje na všetky účty v systéme Windows a profily aplikácie &brandShortName; používajúce túto inštaláciu aplikácie &brandShortName;.">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "Nastavenia…">
<!ENTITY showSettings.accesskey        "N">
<!ENTITY proxiesConfigure.label        "Nastaviť spôsob, akým sa &brandShortName; pripája k sieti Internet">
<!ENTITY connectionsInfo.caption       "Pripojenie">
<!ENTITY offlineInfo.caption           "Režim offline">
<!ENTITY offlineInfo.label             "Konfigurácia nastavení režimu offline">
<!ENTITY showOffline.label             "Nastavenia…">
<!ENTITY showOffline.accesskey         "a">

<!ENTITY Diskspace                       "Miesto na disku">
<!ENTITY offlineCompactFolders.label     "Vykonať údržbu všetkých priečinkov, ak sa celkovo ušetrí aspoň">
<!ENTITY offlineCompactFolders.accesskey "V">
<!ENTITY offlineCompactFoldersMB.label   "MB">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "Vyhradiť do">
<!ENTITY useCacheBefore.accesskey        "h">
<!ENTITY useCacheAfter.label             "MB na disku pre vyrovnávaciu pamäť">
<!ENTITY overrideSmartCacheSize.label    "Vlastné nastavenie vyrovnávacej pamäte">
<!ENTITY overrideSmartCacheSize.accesskey "v">
<!ENTITY clearCacheNow.label             "Vymazať teraz">
<!ENTITY clearCacheNow.accesskey         "e">

<!-- Certificates -->
<!ENTITY certSelection.description       "Pokiaľ stránka požaduje môj osobný certifikát:">
<!ENTITY certs.auto                      "Vybrať automaticky">
<!ENTITY certs.auto.accesskey            "m">
<!ENTITY certs.ask                       "Vždy sa opýtať">
<!ENTITY certs.ask.accesskey             "V">
<!ENTITY enableOCSP.label                "Aktuálnu platnosť certifikátov overovať na serveroch OCSP">
<!ENTITY enableOCSP.accesskey            "A">

<!ENTITY manageCertificates.label "Spravovať certifikáty">
<!ENTITY manageCertificates.accesskey "S">
<!ENTITY viewSecurityDevices.label "Zariadenia">
<!ENTITY viewSecurityDevices.accesskey "Z">
