# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

edit-password-cmd =
    .label = Tùs kùe qúêtz
    .accesskey = e
column-heading-password =
    .label = Taa dizh koo luut ta ne
remove =
    .label = Tee doont naa
    .accesskey = T
close-button =
    .label = TòɁw
    .accesskey = T
show-passwords =
    .label = Ló dizh
    .accesskey = P
hide-passwords =
    .label = kás~lô dizh
    .accesskey = P
remove-all =
    .label = Kíb re-ta
    .accesskey = a
remove-all-shown =
    .label = Kíb re ta nál
    .accesskey = a
remove-all-passwords-title = Kíb retá dízh ré
