# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

title = Xá níe Internet
warning = Ko kúb kúe, nakín thíb sá gósh güíy.
ok = Bliy
http = HTTP
sockets = Sockets
dns = DNS
websockets = WebSockets
refresh = Blíy
auto-refresh = Lí kúb lent xhon segundos
hostname = Pa xhoy
port = Puerto
http2 = HTTP/2
ssl = SSL
active = dob kíy
idle = Na yuy yá
host = Servidor
tcp = TCP
sent = Dá lá
received = KàyáɁ
family = Pá died ná
addresses = Pá xhól
expires = Brúy (segund)
messages-sent = dizh brú
messages-received = Dizh kàyáɁ
bytes-sent = Byte brú
bytes-received = Pló byte kàyáɁ
logging = Gú
log-tutorial = Güí <a data-l10n-name="logging">HTTP Logging</a> for instructions on how to use this tool.
start-logging = Ngót~tòɁ Logging
stop-logging = Ngót~tòɁ Logging
dns-lookup = Kúan DNS
dns-lookup-button = Thus kuey
dns-domain = Pá xöy:
dns-lookup-table-column = IPs
rcwn = RCWN Stats
rcwn-status = Sá dú RCWN
