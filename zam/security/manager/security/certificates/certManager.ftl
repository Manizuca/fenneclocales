# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

certmgr-title =
    .title = Certificate Manager
certmgr-tab-mine =
    .label = Your Certificates
certmgr-tab-servers =
    .label = Servers
certmgr-detail-general-tab-title =
    .label = Xha niey
    .accesskey = X
certmgr-fingerprints-label =
    .value = Fingerprints
certmgr-fingerprints = Fingerprints
certmgr-cert-detail-sha256-fingerprint =
    .value = SHA-256 Fingerprint
certmgr-cert-detail-sha1-fingerprint =
    .value = SHA1 Fingerprint
certmgr-cert-detail-sha-256-fingerprint = SHA-256 Fingerprint
certmgr-cert-detail-sha-1-fingerprint = SHA1 Fingerprint
certmgr-cert-server =
    .label = Server
certmgr-begins-value =
    .value = { certmgr-begins-label.label }
certmgr-expires-value =
    .value = { certmgr-expires-label.label }
certmgr-view =
    .label = Mbwíʔ...
    .accesskey = w
certmgr-delete =
    .label = Tòmbî...
    .accesskey = b
certmgr-delete-builtin =
    .label = Tòmbî...
    .accesskey = b
certmgr-backup =
    .label = M-bì~rè...
    .accesskey = M
exception-mgr-cert-status-view-cert =
    .label = Mbwíʔ...
    .accesskey = V

## PKCS#12 file dialogs

file-browse-pkcs12-spec = PKCS12 yêtz

## Import certificate(s) file dialog


## For editing certificates trust


## For Deleting Certificates


## Cert Viewer

not-present =
    .value = <Not Part Of Certificate>

## Add Security Exception dialog


## Certificate export "Save as" and error dialogs

