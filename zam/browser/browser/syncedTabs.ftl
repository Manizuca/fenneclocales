# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

synced-tabs-sidebar-openprefs = mb-šàlɁ { -sync-brand-short-name } kó lí chél
synced-tabs-context-bookmark-single-tab =
    .label = Tób kùe loo ndó~làz=ná...
    .accesskey = d
synced-tabs-context-copy =
    .label = Lí Chopa
    .accesskey = C
fxa-sign-in = -taɁ lélù { -sync-brand-short-name }
