# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## Font groups by language

fonts-langgroup-arabic =
    .label = Arabigo
fonts-langgroup-armenian =
    .label = Armenio
fonts-langgroup-bengali =
    .label = Bengalí
fonts-langgroup-devanagari =
    .label = Devanagari
fonts-langgroup-ethiopic =
    .label = Etiopia
fonts-langgroup-georgian =
    .label = Georgiano
fonts-langgroup-el =
    .label = Griego
fonts-langgroup-gujarati =
    .label = Gujarati
fonts-langgroup-gurmukhi =
    .label = Gurmukhi
fonts-langgroup-japanese =
    .label = Japones
fonts-langgroup-hebrew =
    .label = Hebreo
fonts-langgroup-kannada =
    .label = Kannada
fonts-langgroup-khmer =
    .label = Khmer
fonts-langgroup-korean =
    .label = Coreano
# Translate "Latin" as the name of Latin (Roman) script, not as the name of the Latin language.
fonts-langgroup-latin =
    .label = Latin
fonts-langgroup-malayalam =
    .label = Malayo
fonts-langgroup-math =
    .label = Matemáticas
fonts-langgroup-sinhala =
    .label = Sinhala
fonts-langgroup-tamil =
    .label = Tamil
fonts-langgroup-telugu =
    .label = Telugu
fonts-langgroup-thai =
    .label = Thaí
fonts-langgroup-tibetan =
    .label = Tibetano
fonts-langgroup-canadian =
    .label = Unified Canadian Syllabary

## Default fonts and their sizes

fonts-default-serif =
    .label = Serif
fonts-default-sans-serif =
    .label = Sans Serif
fonts-minsize-none =
    .label = Nie diif va

## Text Encodings
##
## Translate the encoding names as adjectives for an encoding, not as the name
## of the language.

fonts-languages-fallback-name-cyrillic =
    .label = Cyrillic
fonts-languages-fallback-name-greek =
    .label = Griego
fonts-languages-fallback-name-hebrew =
    .label = Hebreo
fonts-languages-fallback-name-japanese =
    .label = Japónes
fonts-languages-fallback-name-korean =
    .label = Coreano
fonts-languages-fallback-name-thai =
    .label = Thaí
fonts-languages-fallback-name-turkish =
    .label = Turco
fonts-languages-fallback-name-vietnamese =
    .label = Vietnamita
fonts-languages-fallback-name-other =
    .label = Other (incl. Western European)
