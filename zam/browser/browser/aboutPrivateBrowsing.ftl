# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

privatebrowsingpage-open-private-window-label = mb-šàlɁ lô kùb
    .accesskey = k
about-private-browsing-info-bookmarks = Ndó~làz=ná
about-private-browsing-info-searches = kwàɁn
about-private-browsing-info-downloads = b-là yêtz
about-private-browsing-info-cookies = cookies
