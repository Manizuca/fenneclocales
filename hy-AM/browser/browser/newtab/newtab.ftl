# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### Firefox Home / New Tab strings for about:home / about:newtab.

newtab-page-title = Նոր ներդիր

## Search box component.

# "Search" is a verb/action
newtab-search-box-search-button =
    .title = որոնում
    .aria-label = որոնում
newtab-search-box-search-the-web-text = Որոնել առցանց
newtab-search-box-search-the-web-input =
    .placeholder = Որոնել առցանց
    .title = Որոնել առցանց
    .aria-label = Որոնել առցանց

## Top Sites - General form dialog.


## Top Sites - General form dialog buttons. These are verbs/actions.

newtab-topsites-delete-history-button = Ջնջել Պատմությունից

## Top Sites - Delete history confirmation dialog. 

newtab-confirm-delete-history-p1 = Վստահ եք, որ ցանկանում եք ջնջել այս էջի ամեն մի օրինակ ձեր պատմությունից?
# "This action" refers to deleting a page from history.
newtab-confirm-delete-history-p2 = Այս գործողությունը չի կարող վերացվել.

## Context Menu - Action Tooltips.


## Context Menu: These strings are displayed in a context menu and are meant as a call to action for a given page.

newtab-menu-open-new-window = Բացել Նոր Պատուհանով
newtab-menu-open-new-private-window = Բացել Նոր Գաղտնի դիտարկմամբ
newtab-menu-dismiss = Բաց թողնել
newtab-menu-pin = Ամրացնել
newtab-menu-unpin = Ապամրացնել
newtab-menu-delete-history = Ջնջել Պատմությունից
# Bookmark is a noun in this case, "Remove bookmark".
newtab-menu-remove-bookmark = Հեռացնել էջանիշը
# Bookmark is a verb here.
newtab-menu-bookmark = Էջանիշ

## Context Menu - Downloaded Menu. "Download" in these cases is not a verb, 
## it is a noun. As in, "Copy the link that belongs to this downloaded item".


## Context Menu - Download Menu: These are platform specific strings found in the context menu of an item that has
## been downloaded. The intention behind "this action" is that it will show where the downloaded file exists on the file
## system for each operating system.


## Card Labels: These labels are associated to pages to give
## context on how the element is related to the user, e.g. type indicates that
## the page is bookmarked, or is currently open on another device.

newtab-label-visited = Այցելած
newtab-label-bookmarked = Էջանշված
newtab-label-recommended = Թրենդինգ

## Section Menu: These strings are displayed in the section context menu and are 
## meant as a call to action for the given section.


## Section Headers.

newtab-section-header-topsites = Լավագույն կայքեր
newtab-section-header-highlights = Գունանշումներ
# Variables:
#  $provider (String): Name of the corresponding content provider.
newtab-section-header-pocket = Առաջարկվում է { $provider }

## Empty Section States: These show when there are no more items in a section. Ex. When there are no more Pocket story recommendations, in the space where there would have been stories, this is shown instead.


## Pocket Content Section.


## Error Fallback Content.
## This message and suggested action link are shown in each section of UI that fails to render.

