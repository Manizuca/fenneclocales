<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Don't localize these. They're here until they have
     a better place to live. -->
<!ENTITY syncBrand.fullName.label "Firefox Սինք">
<!ENTITY syncBrand.shortName.label "Սինք">

<!-- Main titles. -->
<!ENTITY sync.title.connect.label 'Միանալ &syncBrand.shortName.label;ին'>

<!-- J-PAKE Key Screen -->
<!ENTITY sync.subtitle.connect.label 'Նոր սարքը ակտիվացնելու համար ընտրեք “Կարգավորել &syncBrand.shortName.label;ը” ձեր սարքում։'>
<!ENTITY sync.subtitle.pair.label 'Ակտիվացնելու համար ընտրեք “Կցված սարք” Ձեր այլ սարքում:'>
<!ENTITY sync.pin.default.label '...\n...\n...\n'>
<!ENTITY sync.link.nodevice.label 'Ես ինձ մոտ չունեմ սարք...'>

<!-- Configure Engines -->
<!ENTITY sync.configure.engines.title.passwords2 'Մուտքագրումներ'>
<!ENTITY sync.configure.engines.title.history 'Պատմություն'>
<!ENTITY sync.configure.engines.title.tabs 'Ներդիրներ'>

<!-- Localization note (sync.default.client.name): Default string of the "Device
     name" menu item upon setting up Firefox Sync.  The placeholder &formatS1
     will be replaced by the name of the Firefox release channel and &formatS2
     by the model name of the Android device. Examples look like "Aurora on
     GT-I1950" and "Fennec on MI 2S". -->
<!ENTITY sync.default.client.name '&formatS1;-ը &formatS2;-ում'>

<!-- Bookmark folder strings -->
<!ENTITY bookmarks.folder.menu.label 'Էջանիշերի Ցանկ'>
<!ENTITY bookmarks.folder.places.label ''>
<!ENTITY bookmarks.folder.tags.label 'Կցապիտակներ'>
<!ENTITY bookmarks.folder.toolbar.label 'Էջանիշերի Վահանակ'>
<!ENTITY bookmarks.folder.other.label 'Այլ Էջանիշեր'>
<!ENTITY bookmarks.folder.desktop.label 'Էջանիշեր'>
<!ENTITY bookmarks.folder.mobile.label 'Բջջային էջանիշեր'>
<!-- Pinned sites on about:home. This folder should never be shown to the user, but we have to give it a string name -->
<!ENTITY bookmarks.folder.pinned.label 'Ամրացված է'>

<!-- Firefox Account strings. -->

<!-- Localization note: these are shown in screens after the user has
     created or signed in to an account, and take the user back to
     Firefox. -->
<!ENTITY fxaccount_back_to_browsing 'Անցնել դիտարկմանը'>

<!ENTITY fxaccount_getting_started_welcome_to_sync 'Բարի Գալուստ &syncBrand.shortName.label;'>
<!ENTITY fxaccount_getting_started_description2 'Մուտք գործեք՝ ներդիրները, էջանիշերը, մուտքագրումները համաժամեցնելու համար:'>
<!ENTITY fxaccount_getting_started_get_started 'Սկսել'>
<!ENTITY fxaccount_getting_started_old_firefox 'Օգտագործում եք &syncBrand.shortName.label;ի հին տարբերա՞կը:'>

<!ENTITY fxaccount_status_auth_server 'Հաշվի սպասարկիչը'>
<!ENTITY fxaccount_status_sync_now 'Սինք'>
<!ENTITY fxaccount_status_syncing2 'Սինք...'>
<!ENTITY fxaccount_status_device_name 'Սարքի անունը'>
<!ENTITY fxaccount_status_sync_server 'Սինքի սպասարկիչը'>
<!ENTITY fxaccount_status_needs_verification2 'Ձեր հաշիվը պետք է հաստատել: Կրկին ուղարկել հաստատումը:'>
<!ENTITY fxaccount_status_needs_credentials 'Չհաջողվեց կապակցվել: Սեղմեք`մուտք գործելու համար:'>
<!ENTITY fxaccount_status_needs_upgrade 'Մուտք գործելու համար պետք է արդիացնեք &brandShortName;-ը:'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled '&syncBrand.shortName.label;ը կարգաբերված է, սակայն չի աշխատում: Փոփոխեք \&quot;Auto-sync data\&quot; ցուցիչը Անդրոիդ սարքի կարգավորումենրի &gt; Data Usage-ում:'>
<!ENTITY fxaccount_status_needs_master_sync_automatically_enabled_v21 '&syncBrand.shortName.label;ը կարգաբերված է, սակայն չի աշխատում: Փոփոխեք \&quot;Auto-sync data\&quot; ցուցիչը Անդրոիդ սարքի կարգավորումների &gt; Հաշիվներում:'>
<!ENTITY fxaccount_status_needs_finish_migrating 'Հպվեք՝ Firefox-ի ձեր նոր հաշիվ մուտք գործելու համար:'>
<!ENTITY fxaccount_status_choose_what 'Ընտրեք, թե ինչը համաժամեցնել'>
<!ENTITY fxaccount_status_bookmarks 'Էջանիշեր'>
<!ENTITY fxaccount_status_history 'Պատմություն'>
<!ENTITY fxaccount_status_passwords2 'Մուտքագրումներ'>
<!ENTITY fxaccount_status_tabs 'Բացած ներդիրներ'>
<!ENTITY fxaccount_status_additional_settings 'Լրացուցիչ կարգավորումներ'>
<!ENTITY fxaccount_pref_sync_use_metered2 'Համաժամեցնել միայն Wi-Fi-ով'>
<!-- Localization note: Only affects background syncing, user initiated
     syncs will still be done regardless of the connection -->
<!ENTITY fxaccount_pref_sync_use_metered_summary2 'Կանխել &brandShortName;-ի կողմից համաժամեցումը բջջային կամ մետրային ցանցով'>
<!ENTITY fxaccount_status_legal 'Օրինական' >
<!-- Localization note: when tapped, the following two strings link to
     external web pages.  Compare fxaccount_policy_{linktos,linkprivacy}:
     these strings are separated to accommodate languages that decline
     the two uses differently. -->
<!ENTITY fxaccount_status_linktos2 'Ծառայության պայմանները'>
<!ENTITY fxaccount_status_linkprivacy2 'Գաղտնիության ծանուցում'>
<!ENTITY fxaccount_remove_account 'Անջատել&ellipsis;'>

<!ENTITY fxaccount_remove_account_dialog_title2 'Կապախզվե՞լ Սինքից:'>
<!ENTITY fxaccount_remove_account_dialog_message2 'Դիտարկումների ձեր տվյալները կմնան ձեր սարքում, բայց այն այլևս չի համաժամեցվի ձեր հաշվի հետ:'>
<!-- Localization note: format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_remove_account_toast2 'Firefox-ի &formatS; հաշիվը անջատվել է:'>
<!-- Localization note (fxaccount_remove_account_dialog_action_confirm): This is the label for the
 confirm button in the dialog that shows up when disconnecting from sync. -->
<!ENTITY fxaccount_remove_account_dialog_action_confirm 'Անջատել'>

<!ENTITY fxaccount_enable_debug_mode 'Միացնել Վրիպազերծումը'>

<!-- Localization note: this is the name shown by the Android system
     itself for a Firefox Account. Don't localize this. -->
<!ENTITY fxaccount_account_type_label 'Firefox'>

<!-- Localization note: these are shown by the Android system itself,
     when the user navigates to the Android > Accounts > {Firefox
     Account} Screen. The link takes the user to the Firefox Account
     status activity, which lets them manage their Firefox
     Account. -->
<!ENTITY fxaccount_options_title '&syncBrand.shortName.label;ի Ընտրանքները'>
<!ENTITY fxaccount_options_configure_title 'Կարգավորել &syncBrand.shortName.label;ը'>

<!ENTITY fxaccount_sync_sign_in_error_notification_title2 '&syncBrand.shortName.label;ը կապակցված չէ'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_sign_in_error_notification_text2 'Հպվեք՝ &formatS;-ով մուտք գործելու համար'>

<!ENTITY fxaccount_sync_finish_migrating_notification_title 'Ավարտե՞լ &syncBrand.shortName.label;-ի արդիացումը:'>
<!-- Localization note: the format string below will be replaced
     with the Firefox Account's email address. -->
<!ENTITY fxaccount_sync_finish_migrating_notification_text 'Հպվեք՝ &formatS;-ով մուտք գործելու համար'>
