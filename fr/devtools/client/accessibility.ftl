# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = En savoir plus
accessibility-text-label-header = Noms et étiquettes au format texte

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Avertissement
accessibility-fail =
    .alt = Erreur
accessibility-best-practices =
    .alt = Bonnes pratiques

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Utiliser l’attribut <code>alt</code> pour étiqueter les éléments <div>area</div> qui ont l’attribut <span>href</span>. <a>Pour en savoir plus</a>
