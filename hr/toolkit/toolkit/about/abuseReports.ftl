# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

abuse-report-title-extension = Prijavite ovaj dodatak prema { -vendor-short-name }
abuse-report-title-theme = Prijavite ovu temu prema { -vendor-short-name }
abuse-report-subtitle = Što je problem?
# Variables:
#   $author-name (string) - Name of the add-on author
abuse-report-addon-authored-by = od <a data-l10n-name="author-name">{ $author-name }</a>
abuse-report-submit-description = Opišite problem (opcionalno)

## Panel buttons.

abuse-report-cancel-button = Odustani
abuse-report-next-button = Dalje
abuse-report-goback-button = Idi natrag
abuse-report-submit-button = Pošalji

## Message bars descriptions.


## Variables:
##   $addon-name (string) - Name of the add-on


## Message bars actions.

abuse-report-messagebar-action-remove-extension = Da, ukloni
abuse-report-messagebar-action-keep-extension = Ne, zadržat ću
abuse-report-messagebar-action-remove-theme = Da, ukloni
abuse-report-messagebar-action-keep-theme = Ne, zadržat ću
abuse-report-messagebar-action-retry = Pokušaj ponovno
abuse-report-messagebar-action-cancel = Odustani

## Abuse report reasons (optionally paired with related examples and/or suggestions)

abuse-report-damage-reason = Oštećuje moje računalo i podatke
abuse-report-other-reason = Nešto drugo
