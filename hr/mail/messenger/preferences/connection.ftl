# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

connection-dns-over-https-url-resolver = Koristi pružatelja usluga
    .accesskey = r
# Variables:
#   $name (String) - Display name or URL for the DNS over HTTPS provider
connection-dns-over-https-url-item-default =
    .label = { $name } (zadano)
    .tooltiptext = Koristi zadani URL za DNS preko HTTPS
connection-dns-over-https-url-custom =
    .label = Zadano
    .accesskey = Z
    .tooltiptext = Unesite željeni URL za DNS preko HTTPS
connection-dns-over-https-custom-label = Zadano
