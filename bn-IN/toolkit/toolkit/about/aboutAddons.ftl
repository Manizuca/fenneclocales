# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

addons-window =
    .title = অতিরিক্ত সামগ্রী পরিচালন ব্যবস্থা
search-header-shortcut =
    .key = f
loading-label =
    .value = লোড করা হচ্ছে…
list-empty-installed =
    .value = এই ধরনের কোনো অতিরিক্ত সামগ্রী ইনস্টল করা হয়নি
list-empty-available-updates =
    .value = কোনো আপডেট পাওয়া যায়নি
list-empty-recent-updates =
    .value = কোনো অতিরিক্ত সামগ্রী সম্প্রতি আপডেট করা হয়নি
list-empty-find-updates =
    .label = আপডেটের উপস্থিতি পরীক্ষা করুন
list-empty-button =
    .label = অতিরিক্ত সামগ্রী সম্বন্ধে অধিক তথ্য প্রাপ্ত করুন
install-addon-from-file =
    .label = ফাইল থেকে অতিরিক্ত সামগ্রী ইনস্টল করুন…
    .accesskey = I
tools-menu =
    .tooltiptext = সকল অতিরিক্ত সামগ্রীর জন্য সরঞ্জাম
show-unsigned-extensions-button =
    .label = কিছু এক্সটেনশন যাচাই করা যায়নি
show-all-extensions-button =
    .label = সব এক্সটেনশন প্রদর্শন করুন
debug-addons =
    .label = অ্যাড-অন ডিবাগ
    .accesskey = b
cmd-show-details =
    .label = অতিরিক্ত তথ্য প্রদর্শন
    .accesskey = S
cmd-find-updates =
    .label = আপডেট অনুসন্ধান করুন
    .accesskey = F
cmd-preferences =
    .label =
        { PLATFORM() ->
            [windows] বিকল্প
           *[other] পছন্দ
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
cmd-enable-theme =
    .label = থিম প্রয়োগ করা হবে
    .accesskey = W
cmd-disable-theme =
    .label = থিম প্রয়োগ করা হবে না
    .accesskey = W
cmd-install-addon =
    .label = ইনস্টল
    .accesskey = I
cmd-contribute =
    .label = যোগদান করুন
    .accesskey = C
    .tooltiptext = এই অতিরিক্ত সামগ্রী নির্মাণের কাজে যোগদান করুন
discover-title = অতিরিক্ত সামগ্রী কী?
discover-description = অতিরিক্ত বৈশিষ্ট্য অথবা বিন্যাসের সাহায্যে { -brand-short-name }-কে ব্যবহারকারীর প্রয়োজন অনুযায়ী স্বনির্ধারণের জন্য ব্যবহারযোগ্য অ্যাপ্লিকেশনগুলি অতিরিক্ত সামগ্রী রূপে উপলব্ধ করা হয়। যেমন, সময়-সংরক্ষণকারী সাইড-বার, আবহাওয়ার সূচনাব্যবস্থা অথবা নিজের পছন্দ অনুযায়ী  { -brand-short-name }-এ থিমের ব্যবহার।
discover-footer = ইন্টারনেটের সাথে সংযোগ স্থাপন করা হলে, সর্বোত্তম ও জনপ্রিয় অতিরিক্ত সামগ্রীগুলি  আপনার পরীক্ষার জন্য এই পেইনের মধ্যে প্রদর্শন করা হবে।
detail-version =
    .label = সংস্করণ
detail-last-updated =
    .label = সর্বশেষ আপডেট
detail-contributions-description = এই অতিরিক্ত সামগ্রীর নির্মাণকার্য বজায় রাখার জন্য, এটির নির্মাতার পক্ষ থেকে আপনাকে কিছু পরিমাণ দান করার অনুরোধ জানানো হয়েছে।
detail-update-type =
    .value = স্বয়ংক্রিয় আপডেট
detail-update-default =
    .label = ডিফল্ট
    .tooltiptext = শুধুমাত্র ডিফল্ট মান হিসাবে চিহ্নিত হলে, স্বয়ংক্রিয়ভাবে আপডেট ইনস্টল করা হবে
detail-update-automatic =
    .label = চালু
    .tooltiptext = স্বয়ংক্রিয়ভাবে আপডেট ইনস্টল করুন
detail-update-manual =
    .label = বন্ধ করুন
    .tooltiptext = স্বয়ংক্রিয়ভাবে আপডেট ইনস্টল করা হবে না
detail-home =
    .label = হোম-পেজ
detail-home-value =
    .value = { detail-home.label }
detail-repository =
    .label = অতিরিক্ত সামগ্রীর প্রোফাইল
detail-repository-value =
    .value = { detail-repository.label }
detail-check-for-updates =
    .label = আপডেটের উপস্থিতি পরীক্ষা করুন
    .accesskey = আ
    .tooltiptext = চিহ্নিত অতিরিক্ত সামগ্রীর আপডেট পরীক্ষা করুন
detail-show-preferences =
    .label =
        { PLATFORM() ->
            [windows] বিকল্প
           *[other] পছন্দ
        }
    .accesskey =
        { PLATFORM() ->
            [windows] O
           *[other] P
        }
    .tooltiptext =
        { PLATFORM() ->
            [windows] চিহ্নিত অতিরিক্ত সামগ্রীর বিকল্প মান পরিবর্তন করুন
           *[other] চিহ্নিত অতিরিক্ত সামগ্রীর পছন্দসই মান পরিবর্তন করুন
        }
detail-rating =
    .value = মাত্রা
addon-restart-now =
    .label = অবিলম্বে পুনরারম্ভ করুন
disabled-unsigned-heading =
    .value = কিছু অ্যাড-অন অক্ষম করা হয়েছে
disabled-unsigned-description = নিম্নলিখিত অ্যাড অন { -brand-short-name } ব্যবহারের জন্য যাচাই করা হয় নি। আপনি পারেন <label data-l10n-name="find-addons">প্রতিস্থাপন খুঁজে বের করুন</label> বা তাদের যাচাই করার জন্যে ডেভেলপার দের জিজ্ঞাসা করুন।
disabled-unsigned-learn-more = আপনাকে নিরাপদ রাখার জন্য আমাদের সাহায্য করতে আমাদের প্রচেষ্টা সম্পর্কে আরও জানুন।
disabled-unsigned-devinfo = উৎসাহী ডেভেলপার রা যারা তাদের অ্যাড অন ভেরিফাই করাতে চান তারা পড়তে পারেন আমাদের <label data-l10n-name="learn-more">ব্যবহারকারীর দ্বারা</label>।
extensions-view-discover =
    .name = অতিরিক্ত সামগ্রী প্রাপ্ত করুন
    .tooltiptext = { extensions-view-discover.name }
extensions-view-recent-updates =
    .name = সর্বশেষ আপডেট
    .tooltiptext = { extensions-view-recent-updates.name }
extensions-view-available-updates =
    .name = উপলব্ধ আপডেট
    .tooltiptext = { extensions-view-available-updates.name }

## These are global warnings

extensions-warning-safe-mode-label =
    .value = নিরাপদ মোডে সকল অতিরিক্ত সামগ্রী নিষ্ক্রিয় করা হয়েছে।
extensions-warning-safe-mode-container =
    .tooltiptext = { extensions-warning-safe-mode-label.value }
extensions-warning-check-compatibility-label =
    .value = অতিরিক্ত সামগ্রীর সুসংগতি পরীক্ষা নিষ্ক্রিয় করা হয়েছে। বিসংগত অতিরিক্ত সামগ্রী উপস্থিত থাকতে পারে।
extensions-warning-check-compatibility-container =
    .tooltiptext = { extensions-warning-check-compatibility-label.value }
extensions-warning-check-compatibility-enable =
    .label = সক্রিয় করুন
    .tooltiptext = অতিরিক্ত সামগ্রীর সুসংগতি পরীক্ষার ব্যবস্থা সক্রিয় করা হবে
extensions-warning-update-security-label =
    .value = অতিরিক্ত সামগ্রীর নিরাপত্তা পরীক্ষা নিষ্ক্রিয় করা হয়েছে। আপডেটের ফলে বিপদ দেখা দিতে পারে।
extensions-warning-update-security-container =
    .tooltiptext = { extensions-warning-update-security-label.value }
extensions-warning-update-security-enable =
    .label = সক্রিয় করুন
    .tooltiptext = অতিরিক্ত সামগ্রীর আপডেটের নিরাপত্তা পরীক্ষা ব্যবস্থা সক্রিয় করা হবে

## Strings connected to add-on updates

extensions-updates-check-for-updates =
    .label = আপডেটের উপস্থিতি পরীক্ষা করুন
    .accesskey = C
extensions-updates-view-updates =
    .label = সর্বশেষ আপডেটগুলি দেখুন
    .accesskey = স

# This menu item is a checkbox that toggles the default global behavior for
# add-on update checking.

extensions-updates-update-addons-automatically =
    .label = স্বয়ংক্রিয়ভাবে অতিরিক্ত সামগ্রী আপডেট করা হবে
    .accesskey = A

## Specific add-ons can have custom update checking behaviors ("Manually",
## "Automatically", "Use default global behavior"). These menu items reset the
## update checking behavior for all add-ons to the default global behavior
## (which itself is either "Automatically" or "Manually", controlled by the
## extensions-updates-update-addons-automatically.label menu item).

extensions-updates-reset-updates-to-automatic =
    .label = স্বয়ংক্রিয়ভাবে আপডেট করার উদ্দেশ্যে চিহ্নিত সকল অতিরিক্ত সামগ্রী পুনরায় নির্ধারণ করা হবে
    .accesskey = R
extensions-updates-reset-updates-to-manual =
    .label = ব্যবহারকারী দ্বারা আপডেট করার উদ্দেশ্যে চিহ্নিত সকল অতিরিক্ত সামগ্রী পুনরায় নির্ধারণ করা হবে
    .accesskey = R

## Status messages displayed when updating add-ons

extensions-updates-updating =
    .value = অতিরিক্ত সামগ্রী আপডেট করুন
extensions-updates-installed =
    .value = অতিরিক্ত সামগ্রীগুলি আপডেট করা হয়েছে।
extensions-updates-downloaded =
    .value = অতিরিক্ত সামগ্রীর আপডেটগুলি ডাউনলোড করা হয়েছে।
extensions-updates-restart =
    .label = ইনস্টলেশন সমাপ্ত করার জন্য পুনরায় আরম্ভ করুন
extensions-updates-none-found =
    .value = কোনো আপডেট পাওয়া যায়নি
extensions-updates-manual-updates-found =
    .label = উপলব্ধ সকল আপডেট প্রদর্শন
extensions-updates-update-selected =
    .label = আপডেট ইনস্টল করুন
    .tooltiptext = এই তালিকায় উপলব্ধ আপডেটগুলি ইনস্টল করুন
