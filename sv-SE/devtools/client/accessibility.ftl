# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### These strings are used inside the Accessibility panel.

accessibility-learn-more = Lär dig mer
accessibility-text-label-header = Textetiketter och namn

## Text entries that are used as text alternative for icons that depict accessibility isses.

accessibility-warning =
    .alt = Varning
accessibility-fail =
    .alt = Fel

## Text entries for a paragraph used in the accessibility panel sidebar's checks section
## that describe that currently selected accessible object has an accessibility issue
## with its text label or accessible name.

accessibility-text-label-issue-area = Använd <code>alt</code> attribut för att markera <div>area</div> element som har <span>href</span> attribut. <a>Läs mer</a>
accessibility-text-label-issue-dialog = Dialoger ska vara markerade <a>Läs mer</a>
accessibility-text-label-issue-document-title = Dokument måste ha en <code>titel</code>. <a>Läs mer</a>
accessibility-text-label-issue-glyph = Använd <code>alt</code> attribut för att markera <span>mglyph</span> element <a>Läs mer</a>
accessibility-text-label-issue-heading = Rubriker måste vara markerade. <a>Läs mer</a>
accessibility-text-label-issue-heading-content = Rubriker ska ha synligt textinnehåll. <a>Läs mer</a>
accessibility-text-label-issue-image = Innehåll med bilder måste vara markerade. <a>Läs mer</a>
