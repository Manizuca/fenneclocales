# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


### For this feature, "installation" is used to mean "this discrete download of
### Firefox" and "version" is used to mean "the specific revision number of a
### given Firefox channel". These terms are not synonymous.

title = ज़रूरी सूचना
changed-title = क्या बदला ?
options-title = मेरे क्या विकल्प हैं
resources = साधन
sync-label = ईमेल दर्ज करें
sync-input =
    .placeholder = ईमेल
sync-button = जारी रखे
sync-learn = अधिक जानें
