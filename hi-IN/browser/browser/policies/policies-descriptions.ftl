# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.


## The Enterprise Policies feature is aimed at system administrators
## who want to deploy these settings across several Firefox installations
## all at once. This is traditionally done through the Windows Group Policy
## feature, but the system also supports other forms of deployment.
## These are short descriptions for individual policies, to be displayed
## in the documentation section in about:policies.

policy-AppUpdateURL = मनपसंद ऐप अपडेट URL सेट करें।
policy-DisableAppUpdate = ब्राउज़र को अपडेट होने से रोकें‌‌‍।
# Firefox Screenshots is the name of the feature, and should not be translated.
policy-DisableFirefoxScreenshots = Firefox स्क्रीनशॉट सुविधा को अक्षम करें.
policy-DisableMasterPasswordCreation = यदि सही है, तो एक मास्टर पासवर्ड नहीं बनाया जा सकता है.
policy-DisablePrivateBrowsing = निजी ब्राउजिंग अक्षम करें।
policy-DisableTelemetry = Telemetry बंद करें।
policy-InstallAddonsPermission = कुछ वेबसाइटों को ऐड-ऑन संस्थापित करने की अनुमति दें।
policy-NewTabPage = नया टैब पृष्ठ सक्षम या अक्षम करें।
policy-SSLVersionMax = सबसे अधिक SSL संस्करण देखे।
policy-SSLVersionMin = सबसे कम SSL संस्करण देखे।
